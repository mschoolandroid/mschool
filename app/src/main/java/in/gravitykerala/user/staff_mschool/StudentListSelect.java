package in.gravitykerala.user.staff_mschool;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.Date;
import java.util.List;

public class StudentListSelect extends AppCompatActivity {
    public static String id = null;
    public static String divid = null;
    public static String subid = null;
    // private MobileServiceClient mClient;
    private TextView tv;
    private Studentdapter mAdapter;
    private ProgressBar mProgressBar;
    private SwipeRefreshLayout mSwipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list_select);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isOnline();
                Studentlist();
                mSwipeLayout.setRefreshing(true);
            }
        });
        mSwipeLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeLayout.setRefreshing(true);
            }
        });
        mSwipeLayout.setColorSchemeResources(android.R.color.holo_red_light,
                android.R.color.holo_green_light, android.R.color.holo_orange_light,
                android.R.color.holo_blue_light);
        mSwipeLayout.setEnabled(true);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        AzureMobileService.initialize(this);
        // mProgressBar = (ProgressBar) findViewById(R.id.loadingProgressBar);

        //buttonRefresh = (Button) findViewById(R.id.button_refresh);
        // Initialize the progress bar
        // mProgressBar.setVisibility(ProgressBar.GONE);

//            buttonRefresh.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    refreshItemsFromTable();
//
//                }
        // });


        // Get the Mobile Service Table instance to use
        // mTextNewToDo = (EditText) findViewById(R.id.textNewToDo);

        // Create an adapter to bind the items with the view
        mAdapter = new Studentdapter(this, R.layout.list_student);
        ListView listViewToDo = (ListView) findViewById(R.id.list);
        listViewToDo.setAdapter(mAdapter);

        // Load the items from the Mobile Service
        // refreshItemsFromTable();



   /* String  itemValue    = (String) lv.getItemAtPosition(position);


      Toast.makeText(getApplicationContext(),
         "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
         .show();
       */


        isOnline();
        Studentlist();
    }

    public void Studentlist() {
        List<Pair<String, String>> parameters = new AbstractList<Pair<String, String>>() {
            @Override
            public Pair<String, String> get(int i) {
                return null;
            }

            @Override
            public int size() {
                return 0;
            }
        };
        id = getIntent().getExtras().getString("id");
        divid = getIntent().getExtras().getString("divsionid");
        String date = getIntent().getExtras().getString("date");
        subid = getIntent().getExtras().getString("subid");
        StaffAttendanceParemeterDTO req = new StaffAttendanceParemeterDTO();

        String dateString = date;
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(convertedDate);

        req.eventDate = convertedDate;
        req.divisionId = "" + divid;
        req.subjectId = "" + subid;
        req.studentId = "" + id;
        ListenableFuture<StaffStudentIndividualDayDTO.Single[]> result = AzureMobileService.client.invokeApi("StaffStudentIndividualDayDTO", req, StaffStudentIndividualDayDTO.Single[].class);

        Futures.addCallback(result, new FutureCallback<StaffStudentIndividualDayDTO.Single[]>() {
            @Override
            public void onSuccess(StaffStudentIndividualDayDTO.Single[] result) {
                Log.d("ListenableFuture", "FutureSucess");
                if (result.length == 0) {
                    Toast.makeText(StudentListSelect.this, "no items", Toast.LENGTH_LONG).show();
                    mSwipeLayout.setRefreshing(false);
                } else {
                    for (StaffStudentIndividualDayDTO.Single item : result) {
                        mSwipeLayout.setRefreshing(false);
                        mAdapter.add(item);

                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("ListenableFuture", "FutureFail");
                // mProgressBar.setVisibility(ProgressBar.GONE);
                Toast.makeText(StudentListSelect.this, "exception", Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });
        // mSwipeLayout.setRefreshing(false);
        mAdapter.clear();
    }

    public void markpresent(String item) {
        // setUI(false, false, false, false, false, true, false, true);
        Profilestdid req = new Profilestdid();
        req.studentId = item;
        final ListenableFuture<String> result = AzureMobileService.client.invokeApi("AttendanceUpdate", req, String.class);

        Futures.addCallback(result, new FutureCallback<String>() {
            @Override
            public void onFailure(Throwable exc) {
                //TODO
                exc.printStackTrace();
                Log.d("AttendanceMarking", "Fail:" + result);
                Toast.makeText(StudentListSelect.this, "STATUS CHANGING FAILED, due to some technical issues", Toast.LENGTH_LONG).show();
//                createAndShowDialog((Exception) exc, "Error");
            }

            @Override
            public void onSuccess(String result) {
                //TODO
                Toast.makeText(StudentListSelect.this, "STATUS CHANGING FAILED ," + result, Toast.LENGTH_LONG).show();

            }
        });


    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {

            Toast.makeText(getBaseContext(), "not connected", Toast.LENGTH_LONG).show();
        }
        return false;
    }

}




/*@Override
 public boolean onCreateOptionsMenu(Menu menu) {
  Inflate the menu; this adds items to the action bar if it is present.
 getMenuInflater().inflate(R.menu.yacht, menu);
 return true;
 }
*/




