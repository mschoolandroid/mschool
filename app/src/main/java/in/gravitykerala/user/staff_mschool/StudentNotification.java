package in.gravitykerala.user.staff_mschool;

import java.util.Date;

/**
 * Created by Jasmi on 13-11-2015.
 */
public class StudentNotification {
    @com.google.gson.annotations.SerializedName("id")
    public String sId;


    @com.google.gson.annotations.SerializedName("type")
    public String typ;

    @com.google.gson.annotations.SerializedName("title")
    public String title;

    @com.google.gson.annotations.SerializedName("content")
    public String cnt;

    @com.google.gson.annotations.SerializedName("division_Id")
    public String divid;


    @com.google.gson.annotations.SerializedName("event_Date")
    public Date evdte;
}
