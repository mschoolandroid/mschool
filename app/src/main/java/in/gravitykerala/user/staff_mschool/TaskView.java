package in.gravitykerala.user.staff_mschool;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class TaskView extends AppCompatActivity {
    TaskAsync TaskAsyncTask;
    // private MobileServiceClient mClient;
    private TextView tv;
    private MobileServiceTable<StudentTask.Exam> mTaskTable;
    private TaskAdapter mAdapter;
    private SwipeRefreshLayout mSwipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_view);
        mTaskTable = AzureMobileService.client.getTable("StudentTask", StudentTask.Exam.class);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isOnline();
                refreshTaskFromTable();
                mSwipeLayout.setRefreshing(true);
            }
        });
        mSwipeLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeLayout.setRefreshing(true);
            }
        });
        mSwipeLayout.setColorSchemeResources(android.R.color.holo_red_light,
                android.R.color.holo_green_light, android.R.color.holo_orange_light,
                android.R.color.holo_blue_light);
        mSwipeLayout.setEnabled(true);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        AzureMobileService.initialize(this);

        // mProgressBar = (ProgressBar) findViewById(R.id.loadingProgressBar);

        //buttonRefresh = (Button) findViewById(R.id.button_refresh);
        // Initialize the progress bar
        // mProgressBar.setVisibility(ProgressBar.GONE);

//            buttonRefresh.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    refreshItemsFromTable();
//
//                }
        // });


        // Get the Mobile Service Table instance to use


        // mTextNewToDo = (EditText) findViewById(R.id.textNewToDo);

        // Create an adapter to bind the items with the view
        mAdapter = new TaskAdapter(this, R.layout.list_task);
        ListView listViewToDo = (ListView) findViewById(R.id.list);
        listViewToDo.setAdapter(mAdapter);

        // Load the items from the Mobile Service
        // refreshItemsFromTable();



   /* String  itemValue    = (String) lv.getItemAtPosition(position);


      Toast.makeText(getApplicationContext(),
         "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
         .show();
       */

        //String r1 = listViewToDo.getItemAtPosition(int position).toString();
//        listViewToDo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            public void onItemClick(AdapterView<?> parent , View view,
//                                    int position, long id) {
//
//                        if (result[position].Examtype.equals("University")) {
//                            Intent newActivity0 = new Intent(Result_One.this, A.class);
//
//                            startActivity(newActivity0);
//                        }
//                        else{
//                            Intent newActivity0 = new Intent(Result_One.this, A.class);
//
//                            startActivity(newActivity0);
//                        }


//                    switch (position) {
//                    case 1:
//                        Intent newActivity1 = new Intent(MActivity.this, B.class);
//                        startActivity(newActivity1);
//                        break;
//                    case 2:
//                        Intent newActivity2 = new Intent(MActivity.this, C.class);
//                        startActivity(newActivity2);
//                        break;
//                    case 3:
//                        Intent newActivity3 = new Intent(MActivity.this, D.class);
//                        startActivity(newActivity3);
//                        break;
//
//                    case 4:
//                        Intent newActivity4 = new Intent(MActivity.this, E.class);
//                        startActivity(newActivity4);
//                        break;
//
//                    case 5:
//                        Intent newActivity5 = new Intent(MActivity.this, F.class);
//                        startActivity(newActivity5);
//                        break;
//
//                    case 6:
//                        Intent newActivity6 = new Intent(MActivity.this, G.class);
//                        startActivity(newActivity6);
//                        break;
//
//                    case 7:
//                        Intent newActivity7 = new Intent(MActivity.this, H.class);
//                        startActivity(newActivity7);
//                        break;
//                    case 8:
//                        Intent newActivity8 = new Intent(MActivity.this, I.class);
//                        startActivity(newActivity8);
//                        break;
//                }
        isOnline();
        refreshTaskFromTable();
    }

    public void refreshTaskFromTable() {
        TaskAsyncTask = new TaskAsync();
        TaskAsyncTask.execute();
    }

    public void checkItem(StudentTask.Exam s) {
        s.status = true;
        mTaskTable.update(s);
        refreshTaskFromTable();
    }

    public void uncheckItem(StudentTask.Exam s) {
        s.status = false;
        mTaskTable.update(s);
        refreshTaskFromTable();
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {

            Toast.makeText(getBaseContext(), "not connected", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    private class TaskAsync extends AsyncTask<Void, Void, List<StudentTask.Exam>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            absenteesListview.setVisibility(ListView.GONE);
//            gridViewStudents.setVisibility(GridView.GONE);
//            mProgressBar.setVisibility(ProgressBar.VISIBLE);
        }

        @Override
        protected List<StudentTask.Exam> doInBackground(Void... params) {
            List<StudentTask.Exam> results = null;

            try {
                results = mTaskTable.where().execute().get();

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<StudentTask.Exam> courseDTOs) {
            super.onCancelled(courseDTOs);
            mSwipeLayout.setRefreshing(false);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            mSwipeLayout.setRefreshing(false);
        }

        @Override
        protected void onPostExecute(List<StudentTask.Exam> ClassitemDTO) {
            super.onPostExecute(ClassitemDTO);

            if (ClassitemDTO != null && !ClassitemDTO.isEmpty()) //Successfull results
            {
                mAdapter.dataList = ClassitemDTO;
                for (int i = 0; i < ClassitemDTO.size(); i++) {
                    mAdapter.add(ClassitemDTO.get(i));

                }
                mSwipeLayout.setRefreshing(false);
            } else {
                Toast.makeText(getBaseContext(), "Error retrieving Datas, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }

            mSwipeLayout.setRefreshing(false);
//           spinnerSubject.setVisibility(Spinner.GONE);

        }

    }
    //        });
//    }
//    public void sunil(final StudentTask.Exam item) {
//        if (item.Examtype.equals("University")) {
//            Intent newActivity0 = new Intent(Result_One.this, UniversityExam.class).putExtra("<id>", item.mId);
//            newActivity0.putExtra("id", item.mId);
//            startActivity(newActivity0);
//        } else {
//            Intent newActivity0 = new Intent(Result_One.this, InternalExam.class).putExtra("<id>", item.mId);
//            newActivity0.putExtra("id", item.mId);
//            startActivity(newActivity0);
//        }
//
//    }
}




/*@Override
 public boolean onCreateOptionsMenu(Menu menu) {
  Inflate the menu; this adds items to the action bar if it is present.
 getMenuInflater().inflate(R.menu.yacht, menu);
 return true;
 }
*/




