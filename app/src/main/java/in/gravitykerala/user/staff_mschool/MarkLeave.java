package in.gravitykerala.user.staff_mschool;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.microsoft.windowsazure.mobileservices.http.NextServiceFilterCallback;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilter;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterRequest;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.query.Query;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class MarkLeave extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    public static TextView txtv;
    //private MobileServiceClient mClient;
    public String date = null;
    public Date seldate = new Date();
    MobileServiceTable<StaffLeave> mToDoTable;
    Query mPullQuery;
    Date d = new Date();
    Context currentContext;
    private EditText txtv1;
    private Button btn;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_leave);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        AzureMobileService.initialize(this);
        initializeTables();
        // Get the Mobile Service Table instance to use
        btn = (Button) findViewById(R.id.button);
        txtv = (TextView) findViewById(R.id.editText);
        txtv1 = (EditText) findViewById(R.id.editText2);
        txtv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();

                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        MarkLeave.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
//                setUI(false, false, false, false, true, false, false, true);
                dpd.setAccentColor(Color.parseColor("#3F51B5"));
                dpd.show(getFragmentManager(), "Datepickerdialog");


            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtv.getText().toString().equals("") || txtv1.getText().toString().equals("")) {
                    Snackbar.make(v, "Enter all fields", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MarkLeave.this);
                    alertDialogBuilder.setTitle("mSchool");


                    alertDialogBuilder
                            .setMessage("Click yes to Mark Leave!")
                            .setCancelable(false)

                            .setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            submit();
                                        }
                                    })

                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    dialog.cancel();
                                }
                            });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }
            }

        });
    }

    private void initializeTables() {
        try {
//            StaffDivTable = AzureMobileService.client.getTable("StaffDivision", StaffDivision.class);
//            StudentsTable = AzureMobileService.client.getTable("StaffDivisionStudents", StaffDivisionStudents.class);
            mToDoTable = AzureMobileService.client.getTable("StaffLeave", StaffLeave.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Add a new item
     */

    public void submit() {


        if (AzureMobileService.client == null) {
            return;
        }

        // Create a new item
        final StaffLeave item = new StaffLeave();
        String dateString = txtv.getText().toString();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(convertedDate);

        item.title = (txtv.getText().toString());
        item.evdte = convertedDate;
        item.cnt = (txtv1.getText().toString());

        // item.setComplete(false);

        // Insert the new item

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final StaffLeave entity = mToDoTable.insert(item).get();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MarkLeave.this);
                            alertDialogBuilder.setTitle("Mark Leave");


                            alertDialogBuilder
                                    .setMessage("Leave successfully sent")
                                    .setCancelable(false)

//                                    .setPositiveButton("Yes",
//                                            new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    moveTaskToBack(false);
//                                                    submit();
//                                                }
//                                            })

                                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                            txtv.setText("");
                            txtv1.setText("");
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
//                    createAndShowDialog(e, "Error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MarkLeave.this);
                            alertDialogBuilder.setTitle("Error!");


                            alertDialogBuilder
                                    .setMessage("Marking leave failed. Check your internet connectivity")
                                    .setCancelable(false)

//                                    .setPositiveButton("Yes",
//                                            new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    moveTaskToBack(false);
//                                                    submit();
//                                                }
//                                            })

                                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alertDialog1 = alertDialogBuilder.create();
                            alertDialog1.show();
                            //setUI(false, true, true, true, true);

                        }
                    });
                }
//                setUI(false,true,true,true,true);
                return null;
            }

        }.execute();


    }

    private void createAndShowDialog(final String message, final String title) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MarkLeave.this);

        builder.setMessage(message);
        builder.setTitle(title);
        builder.create().show();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = dayOfMonth + "/" + (++monthOfYear) + "/" + year;
//        seldate = new Date(year - 1900, (monthOfYear - 1), dayOfMonth);

        GregorianCalendar cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
        cal.clear();
        cal.set(year, monthOfYear - 1, dayOfMonth);
        seldate = new Date(cal.getTimeInMillis());

//        Log.d("SelectedDate_Normal:", seldate.toString());
//        Log.d("SelectedDate_GMT:", seldate.toGMTString());
//        Log.d("SelectedDate_Locale:", seldate.toLocaleString());

        if (d.compareTo(seldate) > 0) {
            Toast.makeText(MarkLeave.this, " PLEASE SELECT GENUINE DATE", Toast.LENGTH_LONG).show();
        } else {
            txtv.setText(date);

//            setUI(false, false, false, false, true, false, false, true, false);
        }

    }


    private class ProgressFilter implements ServiceFilter {

        @Override
        public ListenableFuture<ServiceFilterResponse> handleRequest(ServiceFilterRequest request, NextServiceFilterCallback nextServiceFilterCallback) {

            final SettableFuture<ServiceFilterResponse> resultFuture = SettableFuture.create();


            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (mProgressBar != null) mProgressBar.setVisibility(ProgressBar.VISIBLE);
                }
            });

            ListenableFuture<ServiceFilterResponse> future = nextServiceFilterCallback.onNext(request);

            Futures.addCallback(future, new FutureCallback<ServiceFilterResponse>() {
                @Override
                public void onFailure(Throwable e) {
                    resultFuture.setException(e);
                }

                @Override
                public void onSuccess(ServiceFilterResponse response) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            if (mProgressBar != null) mProgressBar.setVisibility(ProgressBar.GONE);
                        }
                    });

                    resultFuture.set(response);
                }
            });

            return resultFuture;
        }
    }
}
