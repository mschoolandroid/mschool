package in.gravitykerala.user.staff_mschool;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.microsoft.windowsazure.mobileservices.MobileServiceException;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOrder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;


public class DateWiseAttFragment extends Fragment {
    public static String selectsubId = "none";
    public static String selectedDivId = "none";
    Spinner spinnerDivision, spinnerSubject, spinnerSemester, spinnerMonth;
    String selectMonthId = null;
    String selectedSemId = "none";
    LinearLayout class_layout, subject_layout, month_layout;
    AsyncSubjectLoader SubAsyncTask;
    AsyncDivisionLoader DivAsyncTask;
    AsyncMonth MonthAsyncTask;

    Context mcontext;
    private ProgressBar mProgressBar;

    private MobileServiceTable<SemesterMonth> MonthTable;
    private MobileServiceTable<StaffDivision> StaffDivTable;
    private MobileServiceTable<StaffDivisionStudents> StudentsTable;
    private SwipeRefreshLayout mSwipeLayout;
    private listAdapterSingle mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_classwise, container, false);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        mSwipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        setUI(false, false, false, false);
        class_layout = (LinearLayout) rootView.findViewById(R.id.class_layout_classwise);
        subject_layout = (LinearLayout) rootView.findViewById(R.id.subject_layout_classwise);
        month_layout = (LinearLayout) rootView.findViewById(R.id.month_layout_classwise);
        spinnerDivision = (Spinner) rootView.findViewById(R.id.spinner_div);
        spinnerSubject = (Spinner) rootView.findViewById(R.id.spinner_sub);
//        spinnerSemester = (Spinner) rootView.findViewById(R.id.spinner7);
        spinnerMonth = (Spinner) rootView.findViewById(R.id.spinner_mon);
        AzureMobileService.initialize(getActivity());
        initializeTables();
        refreshDivisionFromTable();
        spinnerDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                if (position == 0) {
//                    setUI(false,true,false,false);
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(getActivity(), "Select any class", Toast.LENGTH_LONG).show();
                } else {
                    selectedDivId = (String) arg1.getTag();
//                    setUI(false,true,true,false);
//                    Toast.makeText(getActivity(), selectedDivId, Toast.LENGTH_LONG).show();
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, resultStringList);
                    refreshSubjectFromTable();
                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                if (position == 0) {
//                    setUI(false,true,true,false);

                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(getActivity(), "Select any subject", Toast.LENGTH_LONG).show();
                } else {

                    selectsubId = (String) arg1.getTag();
//                    setUI(false,true,true,true);
//                    Toast.makeText(getActivity(), selectsubId, Toast.LENGTH_LONG).show();
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, resultStringList);
                    refreshMonthFromTable();
                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                if (position == 0) {

                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(getActivity(), "Select any subject", Toast.LENGTH_LONG).show();
                } else {

                    selectMonthId = arg1.getTag().toString();

//                    Toast.makeText(getActivity(), "" + selectMonthId, Toast.LENGTH_LONG).show();
                    AbsentListView();
                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);

//        if (toolbar != null) {
//            setSupportActionBar(toolbar);
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        }

        // mProgressBar = (ProgressBar) findViewById(R.id.loadingProgressBar);

        //buttonRefresh = (Button) findViewById(R.id.button_refresh);
        // Initialize the progress bar
        // mProgressBar.setVisibility(ProgressBar.GONE);

//            buttonRefresh.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    refreshItemsFromTable();
//
//                }
        // });


        // Get the Mobile Service Table instance to use
        // mTextNewToDo = (EditText) findViewById(R.id.textNewToDo);

        // Create an adapter to bind the items with the view
        mAdapter = new listAdapterSingle(getActivity(), R.layout.list_datewise_student);
        ListView listViewToDo = (ListView) rootView.findViewById(R.id.listView2);
        listViewToDo.setAdapter(mAdapter);
        return rootView;
    }

    //    public boolean isOnline() {
//        ConnectivityManager cm = (ConnectivityManager) mcontext.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo netInfo = cm.getActiveNetworkInfo();
//        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
//            return true;
//        } else {
//
//            Toast.makeText(getActivity(), "not connected", Toast.LENGTH_LONG).show();
//        }
//        return false;
//    }
    private void initializeTables() {
        try {
            StaffDivTable = AzureMobileService.client.getTable("StaffDivision", StaffDivision.class);
            StudentsTable = AzureMobileService.client.getTable("StaffDivisionStudents", StaffDivisionStudents.class);

            MonthTable = AzureMobileService.client.getTable("SemesterMonth", SemesterMonth.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refreshSubjectFromTable() {
        SubAsyncTask = new AsyncSubjectLoader();
        SubAsyncTask.execute();
    }

    public void refreshDivisionFromTable() {
        DivAsyncTask = new AsyncDivisionLoader();
        DivAsyncTask.execute();
    }


    public void refreshMonthFromTable() {
        MonthAsyncTask = new AsyncMonth();
        MonthAsyncTask.execute();
    }

    private void setUI(Boolean classVisible, Boolean subjectLayoutVisible, Boolean monthVisible, Boolean progressVisible) {


        if (class_layout != null) {   //Check that UI is assigned
            if (classVisible) {
                class_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                class_layout.setVisibility(LinearLayout.GONE);
            }
        }
        if (subject_layout != null) {   //Check that UI is assigned
            if (subjectLayoutVisible) {
                subject_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                subject_layout.setVisibility(LinearLayout.GONE);
            }
        }
        if (month_layout != null) {   //Check that UI is assigned
            if (monthVisible) {
                month_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                month_layout.setVisibility(LinearLayout.GONE);
            }
        }
        if (mProgressBar != null) {   //Check that UI is assigned
            if (progressVisible) {
                mProgressBar.setVisibility(LinearLayout.VISIBLE);
            } else {
                mProgressBar.setVisibility(LinearLayout.GONE);
            }
        }

    }

    public void AbsentListView() {
//        setUI(false, false, false, false, false, true, false, true);


        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                isOnline();
                AbsentListView();
                mSwipeLayout.setRefreshing(true);
            }
        });
        mSwipeLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeLayout.setRefreshing(true);
            }
        });
        mSwipeLayout.setColorSchemeResources(android.R.color.holo_red_light,
                android.R.color.holo_green_light, android.R.color.holo_orange_light,
                android.R.color.holo_blue_light);
        mSwipeLayout.setEnabled(true);
        List<Pair<String, String>> parameters = new AbstractList<Pair<String, String>>() {

            @Override
            public Pair<String, String> get(int i) {
                return null;
            }

            @Override
            public int size() {
                return 0;
            }
        };

        final StaffAttendanceParemeterDTO item = new StaffAttendanceParemeterDTO();
        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
        Date date1 = new Date();
        try {
            date1 = format.parse(selectMonthId);
            System.out.println(selectMonthId);
        } catch (ParseException e) {
// TODO Auto-generated catch block
            e.printStackTrace();
        }
        item.eventDate = date1;
        item.subjectId = selectsubId;
        item.divisionId = selectedDivId;
        ListenableFuture<StaffDivisionAttendance.Single[]> result = AzureMobileService.client.invokeApi("StaffDivisionAttendance", item, StaffDivisionAttendance.Single[].class);

        Futures.addCallback(result, new FutureCallback<StaffDivisionAttendance.Single[]>() {
            @Override
            public void onSuccess(StaffDivisionAttendance.Single[] result) {
                Log.d("ListenableFuture", "FutureSucess");
                if (result.length == 0) {
                    Toast.makeText(getActivity(), "No items", Toast.LENGTH_LONG).show();
                    mSwipeLayout.setRefreshing(false);
                } else {
                    for (StaffDivisionAttendance.Single item : result) {
                        mSwipeLayout.setRefreshing(false);
                        mAdapter.add(item);

                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("ListenableFuture", "FutureFail");
                // mProgressBar.setVisibility(ProgressBar.GONE);
                Toast.makeText(getActivity(), "exception", Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });
        mAdapter.clear();

    }

    @Override
    public void onDestroy() {
        super.onDestroyView();

        try {
            if (SubAsyncTask != null)
                SubAsyncTask.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (DivAsyncTask != null)
                DivAsyncTask.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (MonthAsyncTask != null)
                MonthAsyncTask.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private class AsyncSubjectLoader extends AsyncTask<Void, Void, List<StaffDivision>> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            setUI(true, false, false, true);
        }

        @Override
        protected List<StaffDivision> doInBackground(Void... params) {
            List<StaffDivision> results = null;
            try {
                results = StaffDivTable.where().field("Division_Id").eq(selectedDivId).execute().get();

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<StaffDivision> courseDTOs) {
            super.onCancelled(courseDTOs);
            setUI(true, false, false, false);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            setUI(true, false, false, false);
        }

        @Override
        protected void onPostExecute(List<StaffDivision> departmentItemDTOs) {
            super.onPostExecute(departmentItemDTOs);

            if (departmentItemDTOs != null && !departmentItemDTOs.isEmpty()) //Successfull results
            {
                StaffDivision dummySelector = new StaffDivision(); //Used for showing the first element to be a prompt
                dummySelector.Subname = "Select your Subject";
                // dummySelector.courseId = "none";
                departmentItemDTOs.add(0, dummySelector);

                StaffDivAdapter ClssAdapter = new StaffDivAdapter(getActivity(), R.layout.spinner_selector, R.layout.spinner_dropdown, departmentItemDTOs);
                spinnerSubject.setAdapter(ClssAdapter);
//                sp_Department.setSelection(-1);

                // Toast.makeText(getBaseContext(), "Please Select your Course", Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(getActivity(), "Error retrieving courses, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }
            setUI(true, true, false, false);


        }
    }

    private class AsyncDivisionLoader extends AsyncTask<Void, Void, List<StaffDivision>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            setUI(false, false, false, true);
        }

        @Override
        protected List<StaffDivision> doInBackground(Void... params) {
            List<StaffDivision> results = null;
            try {
                try {
                    results = StaffDivTable.execute().get();
                } catch (MobileServiceException e) {
                    e.printStackTrace();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<StaffDivision> courseDTOs) {
            super.onCancelled(courseDTOs);
            setUI(false, false, false, false);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            setUI(false, false, false, false);
        }

        @Override
        protected void onPostExecute(List<StaffDivision> ClassitemDTO) {
            super.onPostExecute(ClassitemDTO);

            if (ClassitemDTO != null && !ClassitemDTO.isEmpty()) //Successfull results
            {
                StaffDivision dummySelector = new StaffDivision(); //Used for showing the first element to be a prompt
                dummySelector.DivName = "Select your Class";
                dummySelector.Divid = "none";
                ClassitemDTO.add(0, dummySelector);

                DivAdapter depAdapter = new DivAdapter(getActivity(), R.layout.spinner_selector, R.layout.spinner_dropdown, ClassitemDTO);
                spinnerDivision.setAdapter(depAdapter);
//                sp_Department.setSelection(-1);


            } else {
                Toast.makeText(getActivity(), "Error retrieving courses, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }
            setUI(true, false, false, false);
        }
    }

    private class AsyncMonth extends AsyncTask<Void, Void, List<SemesterMonth>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            absenteesListview.setVisibility(ListView.GONE);
//            gridViewStudents.setVisibility(GridView.GONE);
            setUI(true, true, false, true);
        }

        @Override
        protected List<SemesterMonth> doInBackground(Void... params) {
            List<SemesterMonth> results = null;
            try {
                results = MonthTable.where().field("Semester_Id").eq(selectedDivId).orderBy("Month_Date", QueryOrder.Ascending).execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<SemesterMonth> courseDTOs) {
            super.onCancelled(courseDTOs);
            setUI(true, true, false, false);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            setUI(true, true, false, false);
        }

        @Override
        protected void onPostExecute(List<SemesterMonth> ClassitemDTO) {
            super.onPostExecute(ClassitemDTO);

            if (ClassitemDTO != null && !ClassitemDTO.isEmpty()) //Successfull results
            {
                SemesterMonth dummySelector = new SemesterMonth(); //Used for showing the first element to be a prompt
                dummySelector.mname = "Select your Month";
                dummySelector.mId = "none";
                ClassitemDTO.add(0, dummySelector);


                MonthAdapter depAdapter = new MonthAdapter(getActivity(), R.layout.spinner_selector, R.layout.spinner_dropdown, ClassitemDTO);
                spinnerMonth.setAdapter(depAdapter);
//                sp_Department.setSelection(-1);


            } else {
                Toast.makeText(getActivity(), "Error retrieving Datas, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }

            setUI(true, true, true, false);
//           spinnerSubject.setVisibility(Spinner.GONE);

        }

    }

}