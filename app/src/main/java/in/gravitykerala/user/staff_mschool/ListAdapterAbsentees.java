package in.gravitykerala.user.staff_mschool;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import in.gravitykerala.user.staff_mschool.R;

/**
 * Adapter to bind a ToDoItem List to a view
 */
public class ListAdapterAbsentees extends ArrayAdapter<StaffDivisionStudents> {

    /**
     * Adapter context
     */
    Context mContext;


    /**
     * Adapter View layout
     */
    int mLayoutResourceId;

    public ListAdapterAbsentees(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mContext = context;
        mLayoutResourceId = layoutResourceId;
    }

    /**
     * Returns the view for a specific item on the list
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        final StaffDivisionStudents currentItem = getItem(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(mLayoutResourceId, parent, false);
        }

        row.setTag(currentItem);
        TextView tv1 = (TextView) row.findViewById(R.id.textView2);
        tv1.setText(currentItem.rolno + "\t" + currentItem.StudentName);
        return row;
    }
}
