package in.gravitykerala.user.staff_mschool;

import java.util.ArrayList;

/**
 * Created by USER on 12/10/2015.
 */
public class StaffAbsentStudents {
    @com.google.gson.annotations.SerializedName("id")
    public String mId;
    ArrayList<Single> std;

    StaffAbsentStudents() {
        std = new ArrayList<Single>();

    }


    public class Single {
        @com.google.gson.annotations.SerializedName("id")
        public String mId;

        @com.google.gson.annotations.SerializedName("present")
        public Boolean status;

        @com.google.gson.annotations.SerializedName("student_Name")
        public String studnam;

        @com.google.gson.annotations.SerializedName("day")
        public String day;


    }


}
