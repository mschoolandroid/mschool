package in.gravitykerala.user.staff_mschool;
/**
 * Created by Prakash on 6/5/2015.
 * expect112@gmail.com
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import in.gravitykerala.user.staff_mschool.R;

import com.microsoft.windowsazure.notifications.NotificationsHandler;

import java.util.HashSet;
import java.util.Set;

public class PushNotificationHandler extends NotificationsHandler implements GravitySupport {


    public static final int NOTIFICATION_ID = 1;
    NotificationCompat.Builder builder;
    Context ctx, currentContext;
    SharedPreferences prefs;
    private NotificationManager mNotificationManager;

    public static void storePushNotificationTags(StaffProfileDTO profile, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(KEY_PREFERENCE_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(KEY_NOTIFICATION_USER_ID, profile.mid);
        editor.putString(KEY_NOTIFICATION_DEPARTMENT_ID, profile.depid);


        editor.apply();
    }

    @Override
    public void onRegistered(final Context context, final String gcmRegistrationId) {
        super.onRegistered(context, gcmRegistrationId);
        currentContext = context;

        new AsyncTask<Void, Void, Void>() {

            protected Void doInBackground(Void... params) {
                try {
//                    prefs = currentContext.getSharedPreferences(KEY_PREFERENCE_ID, Context.MODE_APPEND);
//                    String selectedCourse = prefs.getString(KEY_SELECTED_COURSE_ID, "none");

                    String[] tags = getSubscribedNotifications(currentContext);
                    for (String tag : tags) {
                        Log.d("IncludedTag:", tag);
                    }
//                    AzureMobileService.initialize(context);
                    AzureMobileService.client.getPush().register(gcmRegistrationId, tags);

                    return null;
                } catch (Exception e) {
                    // handle error
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    @Override
    public void onReceive(Context context, Bundle bundle) {
        Log.d("PushNotification", "Incoming");
        ctx = context;
        String notificationTitle = bundle.getString(KEY_NOTIFICATION_TITLE);
        String notificationContent = bundle.getString(KEY_NOTIFICATION_CONTENT);
        String notificationId = bundle.getString(KEY_NOTIFICATION_ID);
        String notificationType = bundle.getString(KEY_NOTIFICATION_TYPE);
        String notificationMessageType = bundle.getString(KEY_NOTIFICATION_MESSEAGE_TYPE);

        Log.d("notificationId", notificationId);

        Log.d("notificationTitle", notificationTitle);
        Log.d("NotificationContent", notificationContent);
        Log.d("NotificationType", notificationType);
//        Log.d("notificationMessageType", notificationMessageType);

        sendNotification(notificationTitle, notificationContent, notificationType);
    }

    private void sendNotification(String msgTitle, String msgContent, String notificationType) {
        mNotificationManager = (NotificationManager)
                ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntent = new Intent(ctx, HomePage.class);
        notificationIntent.putExtra("fragment", notificationType);

        PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT); //Note: Flag is required if intent needs to be updated for existing notification;PendingIntent.FLAG_UPDATE_CURRENT
//Define sound URI
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(ctx)
                        .setSmallIcon(R.drawable.icon_staff)
                        .setContentTitle("mCampus Staff")
                        .setSound(soundUri) //This sets the sound to play
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msgTitle))
                        .setContentText(msgTitle + ": \n" + msgContent);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


    }

    String[] getSubscribedNotifications(Context context) {
        Set<String> subscribedNotiificationsList = new HashSet<String>();

//        subscribedNotiificationsList.add(AzureMobileService.client.getCurrentUser().getUserId());//UserID change direct preference access

        prefs = context.getSharedPreferences(KEY_PREFERENCE_ID, Context.MODE_APPEND);
        subscribedNotiificationsList.add("COMMON:MSCHOOLSTAFF");
        String userId = prefs.getString(AzureMobileService.USERID_PREF, "undefined");
        if (userId != "undefined")
            subscribedNotiificationsList.add(TAG_PREFIX_USER_ID + userId);

//        String UId = prefs.getString(KEY_NOTIFICATION_USER_ID, "none");
//        if (!"none".equals(UId))
//            subscribedNotiificationsList.add(TAG_PREFIX_BRANCH + UId);

//        String departmentId = prefs.getString(KEY_NOTIFICATION_DEPARTMENT_ID, "none");
//        if (!"none".equals(departmentId))
//            subscribedNotiificationsList.add(TAG_PREFIX_DEPARTMENT + departmentId);

//        String busId = prefs.getString(KEY_NOTIFICATION_BUS_ID, "none");
//        if (!"none".equals(busId))
//            subscribedNotiificationsList.add(TAG_PREFIX_BUS + busId);
//
//
//        String divisionId = prefs.getString(KEY_NOTIFICATION_CLASSROOM_ID, "none");
//        if (!"none".equals(divisionId))
//            subscribedNotiificationsList.add(TAG_PREFIX_CLASSROOM + divisionId);
//
//        String semesterId = prefs.getString(KEY_NOTIFICATION_SEMESTER_ID, "none");
//        if (!"none".equals(semesterId))
//            subscribedNotiificationsList.add(TAG_PREFIX_SEMESTER + semesterId);
//
//        String courseId = prefs.getString(KEY_NOTIFICATION_COURSE_ID, "none");
//        if (!"none".equals(courseId))
//            subscribedNotiificationsList.add(TAG_PREFIX_COURSE + courseId);

        return subscribedNotiificationsList.toArray(new String[subscribedNotiificationsList.size()]);
    }


}