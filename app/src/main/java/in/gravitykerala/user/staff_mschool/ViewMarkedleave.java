package in.gravitykerala.user.staff_mschool;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.microsoft.windowsazure.mobileservices.MobileServiceException;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ViewMarkedleave extends AppCompatActivity {
    ListView studentsListview;
    AsyncDivisionLoader LeaveAsyncTask;
    private ProgressBar mProgressBar;
    private LeavelistStudents studentsAdapter;
    private MobileServiceTable<StaffLeave> leaveTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_markedleave);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        studentsAdapter = new LeavelistStudents(ViewMarkedleave.this, R.layout.list_single_leave);
        studentsListview = (ListView) findViewById(R.id.listViewleave);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar_profile);
        studentsListview.setAdapter((ListAdapter) studentsAdapter);
        AzureMobileService.initialize(this);
        initializeTables();
        refreshItemsFromTable();
    }

    public void refreshItemsFromTable() {
        LeaveAsyncTask = new AsyncDivisionLoader();
        LeaveAsyncTask.execute();
    }

    private void initializeTables() {
        try {
            leaveTable = AzureMobileService.client.getTable("leave", StaffLeave.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addStudent(String item) {
        // setUI(false, false, false, false, false, true, false, true);
        ViewLeaveUpdate parameter = new ViewLeaveUpdate();
        parameter.id = item;

        final ListenableFuture<String> result = AzureMobileService.client.invokeApi("LeaveUpdate", parameter, String.class);

        Futures.addCallback(result, new FutureCallback<String>() {
            @Override
            public void onFailure(Throwable exc) {
                //TODO
                exc.printStackTrace();
                Log.d("AttendanceMarking", "Fail:" + result);
//                createAndShowDialog((Exception) exc, "Error");
            }

            @Override
            public void onSuccess(String result) {
                //TODO
                Toast.makeText(ViewMarkedleave.this, result, Toast.LENGTH_LONG).show();

            }
        });


    }

    private class AsyncDivisionLoader extends AsyncTask<Void, Void, List<StaffLeave>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected List<StaffLeave> doInBackground(Void... params) {
            List<StaffLeave> results = null;
            try {
                try {
                    results = leaveTable.execute().get();
                } catch (MobileServiceException e) {
                    e.printStackTrace();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<StaffLeave> courseDTOs) {
            super.onCancelled(courseDTOs);

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

        }

        @Override
        protected void onPostExecute(List<StaffLeave> ClassitemDTO) {
            super.onPostExecute(ClassitemDTO);

            if (ClassitemDTO != null && !ClassitemDTO.isEmpty()) //Successfull results
            {
                for (StaffLeave item : ClassitemDTO)
                    studentsAdapter.add(item);
                mProgressBar.setVisibility(View.GONE);

            } else if (ClassitemDTO.isEmpty()) {
                Toast.makeText(getBaseContext(), "No Leave Entry!", Toast.LENGTH_LONG).show();
                mProgressBar.setVisibility(View.GONE);
            } else {
                Toast.makeText(getBaseContext(), "Error retrieving courses, Probably bad network connection!", Toast.LENGTH_LONG).show();
                mProgressBar.setVisibility(View.GONE);
            }


        }

    }


}
