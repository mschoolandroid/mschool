package in.gravitykerala.user.staff_mschool;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by USER on 9/19/2015.
 */
public class StaffAttendanceParemeterDTO {


    /**
     * division_Id : a2d16bc2437545849ca417202b15a08b
     * subject_Id : ec2062fe1fa04102b9eb2cd16b1002d5
     * event_Date : 2015-11-11T10:52:45.314Z
     * student_Id : 99341778-3d68-487c-a83d-f9ad842a951c
     */

    @SerializedName("division_Id")
    public String divisionId;
    @SerializedName("subject_Id")
    public String subjectId;
    @SerializedName("event_Date")
    public Date eventDate;
    @SerializedName("student_Id")
    public String studentId;
}
