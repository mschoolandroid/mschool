package in.gravitykerala.user.staff_mschool;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOrder;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

public class StudentsingleSelect extends AppCompatActivity {
    public static String id = null;
    public static String divid = null;
    public static String subid = null;
    Spinner spinnerSemester, spinnerSubject, spinnerMonth;
    Context currentContext;
    String selectsubId = "none";
    LinearLayout listlayout, monthlayout;
    ListView gridViewdays;
    Date date;
    AsyncMonth MonthAsyncTask;

    String selectMonthId = null;

    String selectedSemId = "none";
    private PresentDayListAdapter DayAdapter;
    private GridAdapterStudents PresentDaysAdapter;
    private ProgressBar mProgressBar;
    private MobileServiceTable<StaffDivision> StaffDivTable;

    private MobileServiceTable<SemesterMonth> MonthTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studentsingle_select);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String name = getIntent().getExtras().getString("name");
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar11);
        // mProgressBar1 = (ProgressBar) rootView.findViewById(R.id.progressBar2);
        mProgressBar.setVisibility(ProgressBar.GONE);
        // mProgressBar1.setVisibility(ProgressBar.GONE);
//        semlayout = (LinearLayout) findViewById(R.id.sem_layout);
//        sublayout = (LinearLayout) findViewById(R.id.sem_layout);
        monthlayout = (LinearLayout) findViewById(R.id.month_layout);
        listlayout = (LinearLayout) findViewById(R.id.linlayout_grid);
//        spinnerSemester = (Spinner) findViewById(R.id.spinner3);
        spinnerMonth = (Spinner) findViewById(R.id.spinner4);
        // spinnerSubject = (Spinner) findViewById(R.id.spinner5);
        DayAdapter = new PresentDayListAdapter(this, R.layout.grid_single_item);
        gridViewdays = (ListView) findViewById(R.id.gridViewdays);
        gridViewdays.setAdapter(DayAdapter);
        TextView tv = (TextView) findViewById(R.id.textView_name);
        tv.setText(name);
        AzureMobileService.initialize(this);
        // SplashActivity.Storetok(this);
        initializeTables();
        refreshMonthFromTable();
        String id = getIntent().getExtras().getString("id");
        //String rollno=getIntent().getExtras().getString("rono");
        Log.d("id 1", id);
//        Toast.makeText(this, id, Toast.LENGTH_SHORT).show();

        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                if (position == 0) {


                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(StudentsingleSelect.this, "Select any subject", Toast.LENGTH_LONG).show();
                } else {

                    selectMonthId = arg1.getTag().toString();

//                    Toast.makeText(StudentsingleSelect.this, "" + selectMonthId, Toast.LENGTH_LONG).show();
                    getPresentdates();
                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
//        spinnerSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
//                ArrayAdapter<String> spinnerAdapter;
//                if (position == 0) {
//
//                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(StudentsingleSelect.this, "Select any subject", Toast.LENGTH_LONG).show();
//                } else {
//
//                    selectsubId = (String) arg1.getTag();
//                    Toast.makeText(StudentsingleSelect.this, selectsubId, Toast.LENGTH_LONG).show();
//                    refreshMonthFromTable();
//                }
//                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
//                //  spinnerBranch.setAdapter(spinnerAdapter);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });
    }

    private void setUI(Boolean monthLayoutVisible, Boolean gridLayoutVisible, Boolean mainProgressVisible) {

        if (monthlayout != null) {   //Check that UI is assigned
            if (monthLayoutVisible) {
                monthlayout.setVisibility(LinearLayout.VISIBLE);
            } else {
                monthlayout.setVisibility(LinearLayout.GONE);
            }
        }

        if (listlayout != null) { //Check that UI is assigned
            if (gridLayoutVisible) {
                listlayout.setVisibility(LinearLayout.VISIBLE);
            } else {
                listlayout.setVisibility(LinearLayout.GONE);
            }
        }


        if (mProgressBar != null) {  //Check that UI is assigned
            if (mainProgressVisible) {
                mProgressBar.setVisibility(LinearLayout.VISIBLE);
            } else {
                mProgressBar.setVisibility(LinearLayout.GONE);
            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_studentsingle_select, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initializeTables() {
        try {
            StaffDivTable = AzureMobileService.client.getTable("StaffDivision", StaffDivision.class);
            MonthTable = AzureMobileService.client.getTable("SemesterMonth", SemesterMonth.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void refreshMonthFromTable() {
        MonthAsyncTask = new AsyncMonth();
        MonthAsyncTask.execute();
    }

    public void getPresentdates() {
//        mSwipeLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                mSwipeLayout.setRefreshing(true);
//            }
//        });
        setUI(true, false, true);

        List<Pair<String, String>> parameters = new AbstractList<Pair<String, String>>() {
            @Override
            public Pair<String, String> get(int i) {
                return null;
            }

            @Override
            public int size() {
                return 0;
            }
        };

        id = getIntent().getExtras().getString("id");
        divid = getIntent().getExtras().getString("divsionid");

        subid = getIntent().getExtras().getString("subid");
        StaffAttendanceParemeterDTO req = new StaffAttendanceParemeterDTO();

//        String dtStart = selectMonthId;
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
//        try {
//             date = format.parse(dtStart);
//            System.out.println(date);
//
//        } catch (ParseException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }


        String dtStart = selectMonthId;
        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
        try {
            date = format.parse(selectMonthId);
            System.out.println(date);
        } catch (ParseException e) {
// TODO Auto-generated catch block
            e.printStackTrace();
        }
        req.eventDate = date;
        req.divisionId = "" + divid;
        req.subjectId = "" + subid;
        req.studentId = "" + id;

        ListenableFuture<StaffStudentIndividualDTO.Single[]> resutObject = AzureMobileService.client.invokeApi("StaffStudentIndividualDTO", req, StaffStudentIndividualDTO.Single[].class);

        Futures.addCallback(resutObject, new FutureCallback<StaffStudentIndividualDTO.Single[]>() {
            @Override
            public void onSuccess(StaffStudentIndividualDTO.Single[] result) {
                Log.d("ListenableFuture", "FutureSucess");
                if (result.length == 0) {
                    Toast.makeText(StudentsingleSelect.this, "No items", Toast.LENGTH_LONG).show();
                    setUI(true, false, false);
                } else {
                    for (StaffStudentIndividualDTO.Single item : result) {
                        setUI(true, true, false);
                        DayAdapter.add(item);

                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("ListenableFuture", "FutureFail");
                setUI(true, false, false);
                Toast.makeText(StudentsingleSelect.this, "exception", Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });
//            AzureMobileService.client.invokeApi("StaffStudentIndividualDTO", req, StaffStudentIndividualDTO.Single[].class, new ApiOperationCallback<StaffStudentIndividualDTO.Single[]>() {
//                @Override
//                public void onCompleted(StaffStudentIndividualDTO.Single[] result, Exception exception, ServiceFilterResponse response) {
//                    if (exception == null) {
//
//                        if (result.length == 0) {
//                            Toast.makeText(StudentsingleSelect.this, "no items", Toast.LENGTH_LONG).show();
//                        } else {
//                            for (StaffStudentIndividualDTO.Single item : result) {
//                                mProgressBar.setVisibility(ProgressBar.GONE);
//                                DayAdapter.add(item);
//                            }
//                        }
//
//                    } else {
//                        Toast.makeText(StudentsingleSelect.this, "exception", Toast.LENGTH_LONG).show();
//                    }
//                }
//
//
//            });
        DayAdapter.clear();


    }
    //String id = getIntent().getExtras().getString("id");
//        try {
//            SemesterMonth results = MonthTable.where().field("Month_Name").eq(selectMonthId).execute().get().get(0);
//            seldate=results.evdate;
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }


    private class AsyncMonth extends AsyncTask<Void, Void, List<SemesterMonth>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setUI(false, false, true);

//            absenteesListview.setVisibility(ListView.GONE);
//            gridViewStudents.setVisibility(GridView.GONE);
            mProgressBar.setVisibility(ProgressBar.VISIBLE);
        }

        @Override
        protected List<SemesterMonth> doInBackground(Void... params) {
            List<SemesterMonth> results = null;
            divid = getIntent().getExtras().getString("divsionid");
            try {
                results = MonthTable.where().field("Semester_Id").eq(divid).orderBy("Month_Date", QueryOrder.Ascending).execute().get();

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<SemesterMonth> courseDTOs) {
            super.onCancelled(courseDTOs);
            setUI(false, false, false);

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            setUI(false, false, false);

        }

        @Override
        protected void onPostExecute(List<SemesterMonth> ClassitemDTO) {
            super.onPostExecute(ClassitemDTO);

            if (ClassitemDTO != null && !ClassitemDTO.isEmpty()) //Successfull results
            {
                SemesterMonth dummySelector = new SemesterMonth(); //Used for showing the first element to be a prompt
                dummySelector.mname = "Select your Month";
                dummySelector.mId = "none";
                ClassitemDTO.add(0, dummySelector);


                MonthAdapter depAdapter = new MonthAdapter(StudentsingleSelect.this, R.layout.spinner_selector, R.layout.spinner_dropdown, ClassitemDTO);
                spinnerMonth.setAdapter(depAdapter);
//                sp_Department.setSelection(-1);


            } else {
                Toast.makeText(getBaseContext(), "Error retrieving Datas, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }

            setUI(true, false, false);

//           spinnerSubject.setVisibility(Spinner.GONE);

        }

    }


}
