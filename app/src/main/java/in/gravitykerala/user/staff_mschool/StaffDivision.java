package in.gravitykerala.user.staff_mschool;

/**
 * Created by USER on 8/22/2015.
 */
public class StaffDivision {
    @com.google.gson.annotations.SerializedName("id")
    public String mId;

    @com.google.gson.annotations.SerializedName("subject_Name")
    public String Subname;

    @com.google.gson.annotations.SerializedName("currently_Active")
    public Boolean CuntAct;

    @com.google.gson.annotations.SerializedName("tutor_Id")
    public String Tutrid;

    @com.google.gson.annotations.SerializedName("division_Id")
    public String Divid;

    @com.google.gson.annotations.SerializedName("division_Name")
    public String DivName;

    @com.google.gson.annotations.SerializedName("tutor_Name")
    public String TutrName;

    @com.google.gson.annotations.SerializedName("userId")
    public String Uid;

    public void setDivName(String departmentName) {
        this.DivName = departmentName;
    }

    public void setDivid(String id) {
        this.Divid = id;
    }

}
