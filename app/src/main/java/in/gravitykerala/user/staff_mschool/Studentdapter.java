package in.gravitykerala.user.staff_mschool;


/**
 * Created by USER on 12/7/2015.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import in.gravitykerala.user.staff_mschool.R;

/**
 * Adapter to bind a ToDoItem List to a view
 */
public class Studentdapter extends ArrayAdapter<StaffStudentIndividualDayDTO.Single> {

    /**
     * Adapter context
     */
    Context mContext;


    /**
     * Adapter View layout
     */
    int mLayoutResourceId;

    public Studentdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mContext = context;
        mLayoutResourceId = layoutResourceId;
    }

    /**
     * Returns the view for a specific item on the list
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        final StaffStudentIndividualDayDTO.Single currentItem = getItem(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(mLayoutResourceId, parent, false);
        }

        row.setTag(currentItem);
        String status;

        final TextView tvTitle = (TextView) row.findViewById(R.id.studname);
        final TextView tvContent0 = (TextView) row.findViewById(R.id.status);
        final TextView tvContent1 = (TextView) row.findViewById(R.id.Day);
        final Button btn = (Button) row.findViewById(R.id.button4);
        String present = "present";
        if (currentItem.status == null) {
            present = "absent";

        }
        tvTitle.setText(currentItem.studnam);
        tvContent0.setText("Status" + ":" + "\t" + present);
        tvContent1.setText("Session" + ":" + "\t" + currentItem.day);
        btn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mContext instanceof StudentListSelect) {
                    StudentListSelect activity = (StudentListSelect) mContext;
                    activity.markpresent(currentItem.mId);
                }
                return false;
            }
        });

//        tvContent.setOnClickListener(new View.OnClickListener() {
//            //
//            @Override
//            public void onClick(View arg0) {
//                Attendance activity = (Attendance) mContext;
//                activity.Attendance(currentItem);
//            }
//        });
        return row;
    }
}
