package in.gravitykerala.user.staff_mschool;

import java.util.Date;

/**
 * Created by USER on 12/16/2015.
 */
public class Task {
    @com.google.gson.annotations.SerializedName("id")
    public String id;
    @com.google.gson.annotations.SerializedName("title")
    public String title;
    @com.google.gson.annotations.SerializedName("content")
    public String cnt;
    @com.google.gson.annotations.SerializedName("event_Date")
    public Date evdte;
    @com.google.gson.annotations.SerializedName("type")
    public String typ;


    @com.google.gson.annotations.SerializedName("subject_Name")
    public String Subname;

    @com.google.gson.annotations.SerializedName("subject_Id")
    public String Subid;

    @com.google.gson.annotations.SerializedName("teacherName")
    public String Teacher;
    @com.google.gson.annotations.SerializedName("division_Id")
    public String divid;
    @com.google.gson.annotations.SerializedName("day")
    public String Day;
    @com.google.gson.annotations.SerializedName("completed")
    public Boolean status;
}
