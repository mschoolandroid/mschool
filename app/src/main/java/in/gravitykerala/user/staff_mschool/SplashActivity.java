package in.gravitykerala.user.staff_mschool;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.MenuItem;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.BuildConfig;
import in.gravitykerala.user.staff_mschool.R;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import java.util.AbstractList;
import java.util.List;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 1200;
    double versionCode = BuildConfig.VERSION_CODE;


    public void updationcheck(final Context context) {

        {
//            final ProgressDialog progressDialog = new ProgressDialog(SplashActivity.this,
//                    R.style.AppTheme_Dark_Dialog);
//            progressDialog.setIndeterminate(true);
//            progressDialog.setMessage(getString(R.string.authenticating));
//            progressDialog.show();
            List<Pair<String, String>> parameters = new AbstractList<Pair<String, String>>() {
                @Override
                public Pair<String, String> get(int i) {
                    return null;
                }

                @Override
                public int size() {
                    return 0;
                }
            };
            ListenableFuture<VersionCheck> result = AzureMobileService.client.invokeApi("StaffVersionCheck", VersionCheck.class);

            Futures.addCallback(result, new FutureCallback<VersionCheck>() {
                @Override
                public void onFailure(Throwable exc) {
                    exc.printStackTrace();
                    Log.d("Output", "error VersionCheck");
                    //createAndShowDialog((Exception) exc, "Error");
                }

                @Override
                public void onSuccess(VersionCheck result) {
                    Log.d("AppVersion", "" + versionCode);
                    Log.d("LatestVersion", "" + result.Vcode);
                    Log.d("MandatoryVersion", "" + result.mVcode);

//                    Toast.makeText(SplashActivity.this, "" + result.Vcode, Toast.LENGTH_SHORT).show();
                    if (versionCode < result.mVcode) {
                        new CompolsuryUpdate(AzureMobileService.client.getContext());
                    } else if (versionCode < result.Vcode) {
                        new NoCompulsaryUpdate(AzureMobileService.client.getContext());
                    }
                    //progressDialog.dismiss();
                }
            });
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        // AzureMobileService.initialize(this);
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                isOnline();
                AzureMobileService.initialize(SplashActivity.this);
                updationcheck(SplashActivity.this);
            }
        }, SPLASH_TIME_OUT);
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            Toast.makeText(getBaseContext(), "not connected", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
