package in.gravitykerala.user.staff_mschool;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

import in.gravitykerala.user.staff_mschool.R;

public class GreaterDate extends Dialog {
    public GreaterDate(final Context context) {
        super(context);
        setCancelable(false);
        setContentView(R.layout.content_greater_date);
        getWindow().setBackgroundDrawableResource(R.color.primary_light);
        setTitle("mSchool");
        show();
        Button btn = (Button) findViewById(R.id.button9);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, Attendance_marking.class);
                context.startActivity(i);
                ((Activity) context).finish();

            }

        });
    }
}