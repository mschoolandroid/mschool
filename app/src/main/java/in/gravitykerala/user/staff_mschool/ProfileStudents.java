package in.gravitykerala.user.staff_mschool;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.microsoft.windowsazure.mobileservices.MobileServiceException;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOrder;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ProfileStudents extends AppCompatActivity {
    Spinner spinnerDivision;
    LinearLayout class_layout;
    ListView studentsListview;
    AsyncDivisionLoader DivAsyncTask;
    String selectedDivId = "none";
    private ProfilelistStudents studentsAdapter;
    private ProgressBar mProgressBar;
    private MobileServiceTable<StaffDivisionStudents> StudentsTable;
    private MobileServiceTable<StaffDivision> StaffDivTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_students);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        studentsAdapter = new ProfilelistStudents(ProfileStudents.this, R.layout.list_single);
        studentsListview = (ListView) findViewById(R.id.listView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar_profile);
        studentsListview.setAdapter((ListAdapter) studentsAdapter);
        spinnerDivision = (Spinner) findViewById(R.id.spinnerdiv);
        class_layout = (LinearLayout) findViewById(R.id.class_layout_profile);
        AzureMobileService.initialize(this);
        initializeTables();
        refreshDivisionFromTable();
        spinnerDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                try {
                    DivAsyncTask.cancel(true);
                } catch (Exception e) {

                }
                if (position == 0) {
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(ProfileStudents.this, "Select any class", Toast.LENGTH_LONG).show();
//
                } else {
                    selectedDivId = (String) arg1.getTag();
                    Toast.makeText(ProfileStudents.this, "Please Wait", Toast.LENGTH_LONG).show();
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, resultStringList);
                    setUI(true, false, true);
                    getStudentlist();
                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void initializeTables() {
        try {
            StaffDivTable = AzureMobileService.client.getTable("StaffDivision", StaffDivision.class);
            StudentsTable = AzureMobileService.client.getTable("StaffDivisionStudents", StaffDivisionStudents.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refreshDivisionFromTable() {
        DivAsyncTask = new AsyncDivisionLoader();
        DivAsyncTask.execute();
    }

    public void getStudentlist() {
//        mProgressBar.setVisibility(ProgressBar.VISIBLE);
//        mProgressBar.setVisibility(ProgressBar.VISIBLE);
//        setUI(true,true,true,true);
        // Get the items that weren't marked as completed and add them in the
        // adapter

        new AsyncTask<Void, Void, Void>() {

            @Override

            protected Void doInBackground(Void... params) {

                try {

                    final List<StaffDivisionStudents> results =
                            StudentsTable.where().field("Division_Id").eq(selectedDivId).orderBy("roll_No", QueryOrder.Ascending).execute().get();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {


                            // mSwipeLayout.setRefreshing(false);
                            if (results.size() == 0) {
                                Toast.makeText(ProfileStudents.this, "No students", Toast.LENGTH_LONG).show();
                                // mProgressBar.setVisibility(ProgressBar.GONE);
//                                setUI(false,false,false,false);
                            } else {
                                for (StaffDivisionStudents item : results) {
                                    //   mProgressBar.setVisibility(ProgressBar.GONE);

                                    studentsAdapter.add(item);
                                    // pull.setVisibility(View.GONE);
                                }


                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();


                    // createAndShowDialog(e, "Error");


                }
                return null;
            }

            protected void onPostExecute(Void results) {
                super.onPostExecute(results);
                setUI(true, true, false);
//
            }
        }.execute();

        studentsAdapter.clear();
    }

    private void setUI(Boolean classLayoutVisible,
                       Boolean listLayoutVisible, Boolean progressVisible) {


        if (class_layout != null) {   //Check that UI is assigned
            if (classLayoutVisible) {
                class_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                class_layout.setVisibility(LinearLayout.GONE);
            }
        }

        if (studentsListview != null) {   //Check that UI is assigned
            if (listLayoutVisible) {
                studentsListview.setVisibility(LinearLayout.VISIBLE);
            } else {
                studentsListview.setVisibility(LinearLayout.GONE);
            }
        }

        if (mProgressBar != null) {   //Check that UI is assigned
            if (progressVisible) {
                mProgressBar.setVisibility(LinearLayout.VISIBLE);
            } else {
                mProgressBar.setVisibility(LinearLayout.GONE);
            }
        }

    }

    private class AsyncDivisionLoader extends AsyncTask<Void, Void, List<StaffDivision>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setUI(false, false, true);


        }

        @Override
        protected List<StaffDivision> doInBackground(Void... params) {
            List<StaffDivision> results = null;
            try {
                try {
                    results = StaffDivTable.execute().get();
                } catch (MobileServiceException e) {
                    e.printStackTrace();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<StaffDivision> courseDTOs) {
            super.onCancelled(courseDTOs);
            setUI(false, false, false);

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            setUI(false, false, false);

        }

        @Override
        protected void onPostExecute(List<StaffDivision> ClassitemDTO) {
            super.onPostExecute(ClassitemDTO);

            if (ClassitemDTO != null && !ClassitemDTO.isEmpty()) //Successfull results
            {
                StaffDivision dummySelector = new StaffDivision(); //Used for showing the first element to be a prompt
                dummySelector.DivName = "Select your Class";
                dummySelector.Divid = "none";
                ClassitemDTO.add(0, dummySelector);

                DivAdapter depAdapter = new DivAdapter(ProfileStudents.this, R.layout.spinner_selector, R.layout.spinner_dropdown, ClassitemDTO);
                spinnerDivision.setAdapter(depAdapter);
//                sp_Department.setSelection(-1);


            } else {
                Toast.makeText(ProfileStudents.this, "Error retrieving courses, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }
            setUI(true, false, false);

        }
    }

}
