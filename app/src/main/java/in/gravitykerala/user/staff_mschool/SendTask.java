package in.gravitykerala.user.staff_mschool;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.microsoft.windowsazure.mobileservices.MobileServiceException;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

public class SendTask extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    public String date = null;
    public Date seldate = new Date();
    Spinner spinnerDivision, spinnerSubject, spinnertype;
    MobileServiceTable<StudentNotification> mToDoTable;
    LinearLayout typ_layout, class_layout, subject_layout, titlecontent;
    TextView tv;
    Context currentContext;
    Date d = new Date();
    AsyncSubjectLoader StaffDivAsyncTask;
    AsyncDivisionLoader DivAsyncTask;
    String selectsubId = "none";
    String selectedDivId = "none";
    EditText title, content;
    Button submit_btn;
    private ListAdapterStudents studentsAdapter;
    private ProgressBar mProgressBar, mProgressBar1;
    // private ProgressBar mProgressBar1;
    private MobileServiceTable<StaffDivision> StaffDivTable;
    private MobileServiceTable<StaffDivisionStudents> StudentsTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_task);

        currentContext = SendTask.this;
        tv = (TextView) findViewById(R.id.textView5);
        tv.setClickable(true);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar3);
        mProgressBar1 = (ProgressBar) findViewById(R.id.progressBar4);
//        mProgressBar.setVisibility(ProgressBar.GONE);
        setUI(false, false, false, false, false, false);
        // mProgressBar1.setVisibility(ProgressBar.GONE);
        typ_layout = (LinearLayout) findViewById(R.id.linearLayout3);
        class_layout = (LinearLayout) findViewById(R.id.class_layout_sendtask);
        subject_layout = (LinearLayout) findViewById(R.id.sub_layout_sendtask);
        titlecontent = (LinearLayout) findViewById(R.id.layout_title_cntnt);

        title = (EditText) findViewById(R.id.title);
        content = (EditText) findViewById(R.id.content);
        submit_btn = (Button) findViewById(R.id.button_send);
        spinnerSubject = (Spinner) findViewById(R.id.spinnersub);
        spinnerDivision = (Spinner) findViewById(R.id.spinner_ma_clas);
        spinnertype = (Spinner) findViewById(R.id.type);

        studentsAdapter = new ListAdapterStudents(this, R.layout.list_single);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();

                DatePickerDialog dpd = DatePickerDialog.newInstance(SendTask.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
//                setUI(false, false, false, false, true, false, false, true);
                dpd.setAccentColor(Color.parseColor("#3F51B5"));
                dpd.show(getFragmentManager(), "Datepickerdialog");


            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(Attendance_marking.this, "Enter your remarks about " + tv.getText().toString(), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(SendTask.this, ViewTask.class);
                startActivity(i);

            }
        });
        AzureMobileService.initialize(this);

        ArrayAdapter<String> daySpinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.task));
        daySpinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);

        spinnertype.setAdapter(daySpinnerAdapter);
        spinnertype.setSelection(1);
        initializeTables();

        spinnerDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                if (position == 0) {

                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(SendTask.this, "Select any class", Toast.LENGTH_LONG).show();
                } else {
                    selectedDivId = (String) arg1.getTag();

//                    Toast.makeText(SendTask.this, "Enter Notification Title and content", Toast.LENGTH_LONG).show();
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, resultStringList);

                    refreshSubjectFromTable();
                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                if (position == 0) {
//
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(SendTask.this, "Select any subject", Toast.LENGTH_LONG).show();
                    //setUI(true, true, false, false, false, false, false, true);
                } else {

                    selectsubId = (String) arg1.getTag();
                    Calendar now = Calendar.getInstance();
                    int timeOfDay = now.get(Calendar.HOUR_OF_DAY);


                    // setUI(true, true, true, true, false, true, false, true);


                    Toast.makeText(SendTask.this, "Please Wait", Toast.LENGTH_LONG).show();
                    setUI(true, true, true, true, true, false);

                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, resultStringList);

                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        submit_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                setUI(false, false, false, false, false, true);
                if (title.getText().toString().equals("") || content.getText().toString().equals("")) {
                    setUI(true, true, true, true, true, false);
                    Snackbar.make(view, "Enter all fields", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SendTask.this);
                    alertDialogBuilder.setTitle("Are you sure want to Assign this Task?");


                    alertDialogBuilder
                            .setMessage("Click yes to send!")
                            .setCancelable(false)

                            .setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            moveTaskToBack(false);
                                            submit();

                                        }
                                    })

                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    dialog.cancel();
                                }
                            });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag("Datepickerdialog");


        if (dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = dayOfMonth + "/" + (++monthOfYear) + "/" + year;
//        seldate = new Date(year - 1900, (monthOfYear - 1), dayOfMonth);

        GregorianCalendar cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
        cal.clear();
        cal.set(year, monthOfYear - 1, dayOfMonth);
        seldate = new Date(cal.getTimeInMillis());

//        Log.d("SelectedDate_Normal:", seldate.toString());
//        Log.d("SelectedDate_GMT:", seldate.toGMTString());
//        Log.d("SelectedDate_Locale:", seldate.toLocaleString());

        if (d.compareTo(seldate) > 0) {
            new PreviousDate(currentContext);
        } else {
            tv.setText(date);

            refreshDivisionFromTable();
            //setUI(false, false, false, false, true, false, false, true);
        }

    }

    private void setUI(Boolean typlayout, Boolean classLayoutVisible, Boolean subjectLayoutVisible,
                       Boolean TitleContentVisible, Boolean submitButton, Boolean progressVisible) {

        if (typ_layout != null) {   //Check that UI is assigned
            if (typlayout) {
                typ_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                typ_layout.setVisibility(LinearLayout.GONE);
            }
        }
        if (class_layout != null) {   //Check that UI is assigned
            if (classLayoutVisible) {
                class_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                class_layout.setVisibility(LinearLayout.GONE);
            }
        }
        if (subject_layout != null) {   //Check that UI is assigned
            if (subjectLayoutVisible) {
                subject_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                subject_layout.setVisibility(LinearLayout.GONE);
            }
        }
        if (titlecontent != null) {   //Check that UI is assigned
            if (TitleContentVisible) {
                titlecontent.setVisibility(LinearLayout.VISIBLE);
            } else {
                titlecontent.setVisibility(LinearLayout.GONE);
            }
        }
        if (submit_btn != null) {   //Check that UI is assigned
            if (submitButton) {
                submit_btn.setVisibility(LinearLayout.VISIBLE);
            } else {
                submit_btn.setVisibility(LinearLayout.GONE);
            }
        }
        if (mProgressBar != null) {   //Check that UI is assigned
            if (progressVisible) {
                mProgressBar.setVisibility(LinearLayout.VISIBLE);
            } else {
                mProgressBar.setVisibility(LinearLayout.GONE);
            }
        }


    }

    public void subdivids() {
        String subid = selectsubId;
        String divid = selectedDivId;
    }

    public void refreshSubjectFromTable() {
        StaffDivAsyncTask = new AsyncSubjectLoader();
        StaffDivAsyncTask.execute();
    }

    public void refreshDivisionFromTable() {
        DivAsyncTask = new AsyncDivisionLoader();
        DivAsyncTask.execute();
    }


    private void initializeTables() {
        try {
            StaffDivTable = AzureMobileService.client.getTable("StaffDivision", StaffDivision.class);
            StudentsTable = AzureMobileService.client.getTable("StaffDivisionStudents", StaffDivisionStudents.class);
            mToDoTable = AzureMobileService.client.getTable(StudentNotification.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void submit() {


        if (AzureMobileService.client == null) {
            return;
        }

        // Create a new item
        final Task item = new Task();
        item.typ = spinnertype.getSelectedItem().toString();
        item.title = (title.getText().toString());
        item.evdte = seldate;
        item.cnt = (content.getText().toString());
        item.divid = selectedDivId;
        item.Subid = selectsubId;
        final ListenableFuture<String> result = AzureMobileService.client.invokeApi("StudentTaskStaffInsertion", item, String.class);

        Futures.addCallback(result, new FutureCallback<String>() {
            @Override
            public void onFailure(Throwable exc) {
                //TODO
                exc.printStackTrace();
                Log.d("Sending Task", "Fail:" + result);
//                createAndShowDialog((Exception) exc, "Error");
            }

            @Override
            public void onSuccess(String result) {
                //TODO
                new SuccessDailogTaskAssign(currentContext);
                Log.d("Sending Task", "Success:" + result);
                Toast.makeText(SendTask.this, "Success", Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if (exception.getCause() != null) {
            ex = exception.getCause();
        }
        //createAndShowDialog(ex.toString(), title);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (DivAsyncTask != null)
                DivAsyncTask.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class AsyncSubjectLoader extends AsyncTask<Void, Void, List<StaffDivision>> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setUI(false, true, false, false, false, true);


        }

        @Override
        protected List<StaffDivision> doInBackground(Void... params) {
            List<StaffDivision> results = null;
            try {
                results = StaffDivTable.where().field("Division_Id").eq(selectedDivId).execute().get();

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<StaffDivision> courseDTOs) {
            super.onCancelled(courseDTOs);
            setUI(false, true, false, false, false, false);

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            setUI(false, true, false, false, false, false);

        }

        @Override
        protected void onPostExecute(List<StaffDivision> departmentItemDTOs) {
            super.onPostExecute(departmentItemDTOs);

            if (departmentItemDTOs != null && !departmentItemDTOs.isEmpty()) //Successfull results
            {
                StaffDivision dummySelector = new StaffDivision(); //Used for showing the first element to be a prompt
                dummySelector.Subname = "Select your Subject";
                // dummySelector.courseId = "none";
                departmentItemDTOs.add(0, dummySelector);

                StaffDivAdapter ClassAdapter = new StaffDivAdapter(SendTask.this, R.layout.spinner_selector, R.layout.spinner_dropdown, departmentItemDTOs);
                spinnerSubject.setAdapter(ClassAdapter);
                // setUI(true, true, false, false, false, false, false, true);
//                sp_Department.setSelection(-1);

                // Toast.makeText(getBaseContext(), "Please Select your Course", Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(getBaseContext(), "Error retrieving courses, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }
            setUI(false, true, true, false, false, false);


        }
    }


    private class AsyncDivisionLoader extends AsyncTask<Void, Void, List<StaffDivision>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            setUI(false, false, false, false, false, true);
        }

        @Override
        protected List<StaffDivision> doInBackground(Void... params) {
            List<StaffDivision> results = null;
            try {
                try {
                    results = StaffDivTable.execute().get();
                } catch (MobileServiceException e) {
                    e.printStackTrace();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<StaffDivision> courseDTOs) {
            super.onCancelled(courseDTOs);
            setUI(false, false, false, false, false, false);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            setUI(false, false, false, false, false, false);
        }

        @Override
        protected void onPostExecute(List<StaffDivision> ClassitemDTO) {
            super.onPostExecute(ClassitemDTO);

            if (ClassitemDTO != null && !ClassitemDTO.isEmpty()) //Successfull results
            {
                StaffDivision dummySelector = new StaffDivision(); //Used for showing the first element to be a prompt
                dummySelector.DivName = "Select your Class";
                dummySelector.Divid = "none";
                ClassitemDTO.add(0, dummySelector);

                DivAdapter depAdapter = new DivAdapter(SendTask.this, R.layout.spinner_selector, R.layout.spinner_dropdown, ClassitemDTO);
                spinnerDivision.setAdapter(depAdapter);
//                sp_Department.setSelection(-1);

                setUI(false, true, false, false, false, false);
            } else {
                setUI(false, false, false, false, false, false);
                Toast.makeText(SendTask.this, "Error retrieving courses, Probably bad network connection!", Toast.LENGTH_LONG).show();

            }

        }
    }
}
