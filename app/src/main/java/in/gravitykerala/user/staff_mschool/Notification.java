package in.gravitykerala.user.staff_mschool;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.widget.ListView;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import java.util.AbstractList;
import java.util.List;

public class Notification extends AppCompatActivity {
    private static final String KEY_TITLE = "title";
    private NotificationItemAdapter mAdapter;
    private SwipeRefreshLayout mSwipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isOnline();
                getNotificationList();
                mSwipeLayout.setRefreshing(true);
            }
        });
        mSwipeLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeLayout.setRefreshing(true);
            }
        });
        mSwipeLayout.setColorSchemeResources(android.R.color.holo_red_light,
                android.R.color.holo_green_light, android.R.color.holo_orange_light,
                android.R.color.holo_blue_light);
        mSwipeLayout.setEnabled(true);
        mAdapter = new NotificationItemAdapter(this, R.layout.notification_single);
        ListView listViewToDo = (ListView) findViewById(R.id.list_noti);
        listViewToDo.setAdapter(mAdapter);
        isOnline();
        getNotificationList();
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) Notification.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {

            Toast.makeText(Notification.this, "not connected", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    public void getNotificationList() {
        if (isOnline()) {
        mSwipeLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeLayout.setRefreshing(true);
            }
        });
        List<Pair<String, String>> parameters = new AbstractList<Pair<String, String>>() {
            @Override
            public Pair<String, String> get(int i) {
                return null;
            }

            @Override
            public int size() {
                return 0;
            }
        };
        ListenableFuture<StudentNotificationDTO.Exam[]> result = AzureMobileService.client.invokeApi("StaffNotificationDTO", StudentNotificationDTO.Exam[].class);

        Futures.addCallback(result, new FutureCallback<StudentNotificationDTO.Exam[]>() {
            @Override
            public void onFailure(Throwable exc) {
                exc.printStackTrace();
                Log.d("Output", "error");
                //createAndShowDialog((Exception) exc, "Error");
            }

            @Override
            public void onSuccess(StudentNotificationDTO.Exam[] result) {
                String[] r = null;
                String r1 = "";
                for (int i = 0; i < result.length; i++) {
                    Log.d("ID", result[i].content);

                    r1 = r1 + result[i].type + "\t" + result[i].title + "\t" + result[i].content + "\n";
                }
                if (result.length == 0) {
                    mSwipeLayout.setRefreshing(false);
                    Toast.makeText(Notification.this, "no notification", Toast.LENGTH_SHORT).show();
                } else {
                    for (StudentNotificationDTO.Exam item1 : result) {
                        mSwipeLayout.setRefreshing(false);
                        mAdapter.add(item1);
                    }
                }


//                tv.setText(result.getName() + "\t" + result.getphno() + "\t" + result.getEmail()+"\n"+
//                        result.getName() + "\t" + result.getphno() + "\t" + result.getEmail()+"\n"+
//                        result.getName() + "\t" + result.getphno() + "\t" + result.getEmail());
                // refreshItemsFromTable();
            }
        });
        mAdapter.clear();

    }
    }
}
