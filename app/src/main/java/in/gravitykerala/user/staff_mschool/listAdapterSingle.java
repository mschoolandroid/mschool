package in.gravitykerala.user.staff_mschool;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import in.gravitykerala.user.staff_mschool.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Adapter to bind a ToDoItem List to a view
 */
public class listAdapterSingle extends ArrayAdapter<StaffDivisionAttendance.Single> {

    /**
     * .
     * Adapter context
     */
    Context mContext;


    /**
     * Adapter View layout
     */
    int mLayoutResourceId;

    public listAdapterSingle(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mContext = context;
        mLayoutResourceId = layoutResourceId;
    }

    /**
     * Returns the view for a specific item on the list
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        final StaffDivisionAttendance.Single currentItem = getItem(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(mLayoutResourceId, parent, false);
        }

        row.setTag(currentItem);
        String status;

        final TextView tvTitle = (TextView) row.findViewById(R.id.date1);
        final TextView tvContent0 = (TextView) row.findViewById(R.id.strength);
        final TextView tvContent1 = (TextView) row.findViewById(R.id.absent);


        String present = "present";
//        if(currentItem.status==null){
//            present= "absent";
//
//        }


        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date convertedDate = currentItem.evdate;
        String newdt = dateFormat.format(convertedDate);
        try {
            convertedDate = dateFormat.parse(currentItem.evdate.toString());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        tvTitle.setText(newdt);
        tvContent0.setText("Strength" + ":" + "\t" + currentItem.str);
        tvContent1.setText("Absent " + ":" + "\t" + currentItem.absentnum);
        tvContent1.setTextColor(Color.RED);
        final String date = tvTitle.getText().toString();
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, StudentDateSingle.class);
                i.putExtra("date", currentItem.evdate.toString());
                i.putExtra("divsionid", DateWiseAttFragment.selectedDivId);
                i.putExtra("subid", DateWiseAttFragment.selectsubId);
                mContext.startActivity(i);
            }
        });

//        tvContent.setOnClickListener(new View.OnClickListener() {
//            //
//            @Override
//            public void onClick(View arg0) {
//                Attendance activity = (Attendance) mContext;
//                activity.Attendance(currentItem);
//            }
//        });
        return row;
    }
}
