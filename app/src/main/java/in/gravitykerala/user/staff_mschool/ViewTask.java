package in.gravitykerala.user.staff_mschool;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.microsoft.windowsazure.mobileservices.MobileServiceException;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOrder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ViewTask extends AppCompatActivity {
    public static String selectsubId = "none";
    public static String selecttyp = "none";
    public static String selecttitle = "none";
    TextView tv;
    Spinner spinnerDivision, spinnerSubject, spinnertyp, spinnertitle;
    ListView studentsListview;
    LinearLayout class_layout, subject_layout, tasktype_layout, tasktitle_layout;
    ListView listViewToDo;
    Context currentContext;
    AsyncSubjectLoader SubAsyncTask;
    AsyncDivisionLoader DivAsyncTask;
    TaskAsync TaskAsyncTask;
    TitleAsync TitleAsyncTask;
    TasklistAsync TasklistAsyncTask;
    String selectedDivId = "none";
    private SwipeRefreshLayout mSwipeLayout;
    private TaskAdapter mAdapter;
    // private ListAdapterStudents studentsAdapter;
    private ProgressBar mProgressBar;
    // private ProgressBar mProgressBar1;
    private MobileServiceTable<StaffDivision> StaffDivTable;
    private MobileServiceTable<StaffDivisionStudents> StudentsTable;
    private MobileServiceTable<Task> mTaskTable;
    private MobileServiceTable<StudentTask.Exam> mTaskListTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_view2);
        currentContext = this;
        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar_viewtask);
        // mProgressBar1 = (ProgressBar) rootView.findViewById(R.id.progressBar2);
//        mProgressBar.setVisibility(ProgressBar.GONE);
        //  setUI(true, false, false, false);
        // mProgressBar1.setVisibility(ProgressBar.GONE);
        class_layout = (LinearLayout) findViewById(R.id.class_layout_viewtask);
        subject_layout = (LinearLayout) findViewById(R.id.subject_layout_viewtask);
        tasktype_layout = (LinearLayout) findViewById(R.id.tasktype_viewtask);
        tasktitle_layout = (LinearLayout) findViewById(R.id.tasktitle_viewtask);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv = (TextView) findViewById(R.id.duedate);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        spinnerDivision = (Spinner) findViewById(R.id.spinner);
        spinnerSubject = (Spinner) findViewById(R.id.spinner2);
        spinnertyp = (Spinner) findViewById(R.id.spinner9);
        spinnertitle = (Spinner) findViewById(R.id.spinner10);
        // studentsAdapter = new ListAdapterStudents(this, R.layout.list_single);
        //studentsListview = (ListView) findViewById(R.id.list);
        mAdapter = new TaskAdapter(this, R.layout.list_task);
        listViewToDo = (ListView) findViewById(R.id.list);
        listViewToDo.setAdapter(mAdapter);
        //studentsListview.setAdapter((ListAdapter) studentsAdapter);
        AzureMobileService.initialize(this);
        initializeTables();
        refreshDivisionFromTable();
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isOnline();
                refreshTasklistFromTable();
                mSwipeLayout.setRefreshing(true);
            }
        });
        spinnerDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                if (position == 0) {
//                    setUI(false,true,false,false);
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(ViewTask.this, "Select any class", Toast.LENGTH_LONG).show();
                } else {
                    selectedDivId = (String) arg1.getTag();
//                    setUI(false,true,true,false);
//                    Toast.makeText(ViewTask.this, selectedDivId, Toast.LENGTH_LONG).show();
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, resultStringList);
                    refreshSubjectFromTable();
                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                if (position == 0) {
//                    setUI(false,true,true,false);

                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(ViewTask.this, "Select any subject", Toast.LENGTH_LONG).show();
                } else {

                    selectsubId = (String) arg1.getTag();
//                    setUI(false,true,true,true);
//                    Toast.makeText(ViewTask.this, selectsubId, Toast.LENGTH_LONG).show();
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, resultStringList);
                    refreshTaskFromTable();
                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spinnertyp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                if (position == 0) {
//                    setUI(false,true,true,false);

                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(ViewTask.this, "Select any subject", Toast.LENGTH_LONG).show();
                } else {

                    selecttyp = spinnertyp.getSelectedItem().toString();
//                    setUI(false,true,true,true);
//                    Toast.makeText(ViewTask.this, selectsubId, Toast.LENGTH_LONG).show();
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, resultStringList);
                    refreshTitleFromTable();
                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spinnertitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                if (position == 0) {
//                    setUI(false,true,true,false);

                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(ViewTask.this, "Select any subject", Toast.LENGTH_LONG).show();
                } else {

                    selecttitle = spinnertitle.getSelectedItem().toString();
//                    setUI(false,true,true,true);
//                    Toast.makeText(ViewTask.this, selectsubId, Toast.LENGTH_LONG).show();
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, resultStringList);
                    refreshTasklistFromTable();
                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }

    public void checkItem(StudentTask.Exam s) {
        s.status = true;

        mTaskListTable.update(s);
        refreshTasklistFromTable();
    }

    public void uncheckItem(StudentTask.Exam s) {
        s.status = false;
        mTaskListTable.update(s);
        refreshTasklistFromTable();
    }

    public void refreshTasklistFromTable() {
        TasklistAsyncTask = new TasklistAsync();
        TasklistAsyncTask.execute();
    }

    private void initializeTables() {
        try {
            StaffDivTable = AzureMobileService.client.getTable("StaffDivision", StaffDivision.class);
            StudentsTable = AzureMobileService.client.getTable("StaffDivisionStudents", StaffDivisionStudents.class);
            mTaskTable = AzureMobileService.client.getTable("StudentTask", Task.class);
            mTaskListTable = AzureMobileService.client.getTable("StudentTask", StudentTask.Exam.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUI(Boolean classLayoutVisible, Boolean subjectLayoutVisible, Boolean tasktypeLayoutVisible, Boolean tasktitleVisible, Boolean
            DueDateVisible, Boolean listLayoutVisible, Boolean progressVisible) {


        if (class_layout != null) {   //Check that UI is assigned
            if (classLayoutVisible) {
                class_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                class_layout.setVisibility(LinearLayout.GONE);
            }
        }
        if (subject_layout != null) {   //Check that UI is assigned
            if (subjectLayoutVisible) {
                subject_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                subject_layout.setVisibility(LinearLayout.GONE);
            }
        }
        if (tasktype_layout != null) {   //Check that UI is assigned
            if (tasktypeLayoutVisible) {
                tasktype_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                tasktype_layout.setVisibility(LinearLayout.GONE);
            }
        }
        if (tasktitle_layout != null) {   //Check that UI is assigned
            if (tasktitleVisible) {
                tasktitle_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                tasktitle_layout.setVisibility(LinearLayout.GONE);
            }
        }
        if (tv != null) {   //Check that UI is assigned
            if (DueDateVisible) {
                tv.setVisibility(LinearLayout.VISIBLE);
            } else {
                tv.setVisibility(LinearLayout.GONE);
            }
        }
        if (listViewToDo != null) {   //Check that UI is assigned
            if (listLayoutVisible) {
                tv.setVisibility(LinearLayout.VISIBLE);
            } else {
                tv.setVisibility(LinearLayout.GONE);
            }
        }
        if (mProgressBar != null) {   //Check that UI is assigned
            if (progressVisible) {
                mProgressBar.setVisibility(LinearLayout.VISIBLE);
            } else {
                mProgressBar.setVisibility(LinearLayout.GONE);
            }
        }

    }

    public void refreshSubjectFromTable() {
        SubAsyncTask = new AsyncSubjectLoader();
        SubAsyncTask.execute();
    }

    public void refreshTaskFromTable() {
        TaskAsyncTask = new TaskAsync();
        TaskAsyncTask.execute();
    }

    public void refreshTitleFromTable() {
        TitleAsyncTask = new TitleAsync();
        TitleAsyncTask.execute();
    }

    public void refreshDivisionFromTable() {
        DivAsyncTask = new AsyncDivisionLoader();
        DivAsyncTask.execute();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            if (SubAsyncTask != null)
                SubAsyncTask.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (DivAsyncTask != null)
                DivAsyncTask.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {

            Toast.makeText(getBaseContext(), "No Data Connection", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    private class TasklistAsync extends AsyncTask<Void, Void, List<StudentTask.Exam>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setUI(true, true, true, true, false, false, false);
            mAdapter.clear();
            mSwipeLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeLayout.setRefreshing(true);
                }
            });
            mSwipeLayout.setColorSchemeResources(android.R.color.holo_red_light,
                    android.R.color.holo_green_light, android.R.color.holo_orange_light,
                    android.R.color.holo_blue_light);
            mSwipeLayout.setEnabled(true);
//            absenteesListview.setVisibility(ListView.GONE);
//            gridViewStudents.setVisibility(GridView.GONE);
            // mProgressBar.setVisibility(ProgressBar.VISIBLE);
        }

        @Override
        protected List<StudentTask.Exam> doInBackground(Void... params) {
            List<StudentTask.Exam> results = null;

            try {
                results = mTaskListTable.where().field("Subject_Id").eq(selectsubId).and().field("Type").eq(selecttyp).and().field("Title").eq(selecttitle).and().field("Completed").eq(false).orderBy("Target_Name", QueryOrder.Ascending).execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<StudentTask.Exam> courseDTOs) {
            super.onCancelled(courseDTOs);
            mSwipeLayout.setRefreshing(false);
            setUI(true, true, true, true, false, false, false);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            mSwipeLayout.setRefreshing(false);
            setUI(true, true, true, true, false, false, false);
        }

        @Override
        protected void onPostExecute(List<StudentTask.Exam> ClassitemDTO) {
            super.onPostExecute(ClassitemDTO);
            if (ClassitemDTO.size() == 0) {
                Toast.makeText(ViewTask.this, "No incomplete tasks", Toast.LENGTH_LONG).show();

            } else if (ClassitemDTO != null && !ClassitemDTO.isEmpty()) //Successfull results
            {
                mAdapter.dataList = ClassitemDTO;

                for (int i = 0; i < ClassitemDTO.size(); i++) {
                    mAdapter.add(ClassitemDTO.get(i));
                    // Date d= ClassitemDTO.get(i).evdte;
                    //String year = d.getYear()+1900;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date convertedDate = ClassitemDTO.get(i).evdte;
                    String date = dateFormat.format(convertedDate);
                    try {
                        convertedDate = dateFormat.parse(ClassitemDTO.get(i).evdte.toString());
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    tv.setText("Due Date :" + date);
                    tv.setTextColor(Color.RED);

                }
                mSwipeLayout.setRefreshing(false);

            } else {
                Toast.makeText(getBaseContext(), "Error retrieving Datas, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }

            mSwipeLayout.setRefreshing(false);
            setUI(true, true, true, true, true, true, false);
//           spinnerSubject.setVisibility(Spinner.GONE);

        }

    }

    private class AsyncSubjectLoader extends AsyncTask<Void, Void, List<StaffDivision>> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            setUI(true, false, false, false, false, false, true);
        }

        @Override
        protected List<StaffDivision> doInBackground(Void... params) {
            List<StaffDivision> results = null;
            try {
                results = StaffDivTable.where().field("Division_Id").eq(selectedDivId).execute().get();

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<StaffDivision> courseDTOs) {
            super.onCancelled(courseDTOs);
            setUI(true, false, false, false, false, false, false);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            setUI(true, false, false, false, false, false, false);
        }

        @Override
        protected void onPostExecute(List<StaffDivision> departmentItemDTOs) {
            super.onPostExecute(departmentItemDTOs);

            if (departmentItemDTOs != null && !departmentItemDTOs.isEmpty()) //Successfull results
            {
                StaffDivision dummySelector = new StaffDivision(); //Used for showing the first element to be a prompt
                dummySelector.Subname = "Select your Subject";
                // dummySelector.courseId = "none";
                departmentItemDTOs.add(0, dummySelector);

                StaffDivAdapter ClssAdapter = new StaffDivAdapter(ViewTask.this, R.layout.spinner_selector, R.layout.spinner_dropdown, departmentItemDTOs);
                spinnerSubject.setAdapter(ClssAdapter);
//                sp_Department.setSelection(-1);

                // Toast.makeText(getBaseContext(), "Please Select your Course", Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(ViewTask.this, "Error retrieving courses, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }
            setUI(true, true, false, false, false, false, false);


        }
    }

    private class AsyncDivisionLoader extends AsyncTask<Void, Void, List<StaffDivision>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            setUI(false, false, false, false, false, false, true);
        }

        @Override
        protected List<StaffDivision> doInBackground(Void... params) {
            List<StaffDivision> results = null;
            try {
                try {
                    results = StaffDivTable.execute().get();
                } catch (MobileServiceException e) {
                    e.printStackTrace();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<StaffDivision> courseDTOs) {
            super.onCancelled(courseDTOs);
            setUI(false, false, false, false, false, false, false);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            setUI(false, false, false, false, false, false, false);
        }

        @Override
        protected void onPostExecute(List<StaffDivision> ClassitemDTO) {
            super.onPostExecute(ClassitemDTO);

            if (ClassitemDTO != null && !ClassitemDTO.isEmpty()) //Successfull results
            {
                StaffDivision dummySelector = new StaffDivision(); //Used for showing the first element to be a prompt
                dummySelector.DivName = "Select your Class";
                dummySelector.Divid = "none";
                ClassitemDTO.add(0, dummySelector);

                DivAdapter depAdapter = new DivAdapter(ViewTask.this, R.layout.spinner_selector, R.layout.spinner_dropdown, ClassitemDTO);
                spinnerDivision.setAdapter(depAdapter);
//                sp_Department.setSelection(-1);


            } else {
                Toast.makeText(ViewTask.this, "Error retrieving courses, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }
            setUI(true, false, false, false, false, false, false);
        }
    }

    private class TaskAsync extends AsyncTask<Void, Void, List<String>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            setUI(true, true, false, false, false, false, true);
        }

        @Override
        protected List<String> doInBackground(Void... params) {
            List<Task> results = null;
            ArrayList<String> type = new ArrayList<String>();
            String g = null;
            try {
                results = mTaskTable.where().field("Subject_Id").eq(selectsubId).execute().get();
                int c = results.size();
                for (int i = 0; i < results.size(); i++) {
                    if (i == 0) {
                        //  type.add("" + results.size());
                        g = results.get(i).typ;
                        type.add(g);
                    } else {
                        // type.add(""+results.size());
                        g = results.get(i).typ;
                        if (type.contains(g)) {

                        } else {
                            type.add(g);

                        }
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return type;
        }

        @Override
        protected void onCancelled(List<String> courseDTOs) {
            super.onCancelled(courseDTOs);
            setUI(true, true, false, false, false, false, false);

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            setUI(true, true, false, false, false, false, false);

        }

        @Override
        protected void onPostExecute(List<String> ClassitemDTO) {
            super.onPostExecute(ClassitemDTO);

            if (ClassitemDTO != null && !ClassitemDTO.isEmpty()) //Successfull results
            {
                String dummySelector = "Select your Task Type";
                ClassitemDTO.add(0, dummySelector);

                TaskitemAdapter Adapter = new TaskitemAdapter(ViewTask.this, R.layout.spinner_selector, R.layout.spinner_dropdown, ClassitemDTO);
                spinnertyp.setAdapter(Adapter);
//                sp_Department.setSelection(-1);


            } else {
                Toast.makeText(ViewTask.this, "Error retrieving courses, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }
            setUI(true, true, true, false, false, false, false);
        }

    }

    private class TitleAsync extends AsyncTask<Void, Void, List<String>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            setUI(true, true, true, false, false, false, true);
        }

        @Override
        protected List<String> doInBackground(Void... params) {
            List<Task> results = null;
            ArrayList<String> type = new ArrayList<String>();
            String g = null;
            try {

                results = mTaskTable.where().field("Subject_Id").eq(selectsubId).and().field("Type").eq(selecttyp).execute().get();
                int c = results.size();
                for (int i = 0; i < results.size(); i++) {

                    if (type == null) {
                        //  type.add("" + results.size());
                        g = results.get(i).title;
                        type.add(g);

                    } else {
                        // type.add(""+results.size());

                        g = results.get(i).title;
                        if (type.contains(g)) {

                        } else {
                            type.add(g);

                        }

                    }

                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return type;
        }

        @Override
        protected void onCancelled(List<String> courseDTOs) {
            super.onCancelled(courseDTOs);
            setUI(true, true, true, false, false, false, false);

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            setUI(true, true, true, false, false, false, false);

        }

        @Override
        protected void onPostExecute(List<String> ClassitemDTO) {
            super.onPostExecute(ClassitemDTO);
            if (ClassitemDTO == null) {
                Toast.makeText(ViewTask.this, "Nothing to show", Toast.LENGTH_LONG).show();

            } else if (ClassitemDTO != null && !ClassitemDTO.isEmpty()) //Successfull results
            {
                String dummySelector = "Select your Task Type";
                ClassitemDTO.add(0, dummySelector);

                TitleitemAdapter Adapter = new TitleitemAdapter(ViewTask.this, R.layout.spinner_selector, R.layout.spinner_dropdown, ClassitemDTO);
                spinnertitle.setAdapter(Adapter);
//                sp_Department.setSelection(-1);


            } else {
                Toast.makeText(ViewTask.this, "Error retrieving courses, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }
            setUI(true, true, true, true, false, false, false);
        }

    }


}
