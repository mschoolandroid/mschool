package in.gravitykerala.user.staff_mschool;


import java.util.ArrayList;
import java.util.Date;

/**
 * Created by USER on 7/10/2015.
 */
public class StudentTask {
    @com.google.gson.annotations.SerializedName("id")
    public String mId;
    ArrayList<Exam> staffs;

    StudentTask() {
        staffs = new ArrayList<Exam>();

    }

    public Exam getName(int i) {
        return staffs.get(i);
    }

    public String getId() {
        return mId;
    }

    public class Exam {
        @com.google.gson.annotations.SerializedName("id")
        public String id;
        @com.google.gson.annotations.SerializedName("subject_Name")
        public String Subname;
        @com.google.gson.annotations.SerializedName("event_Date")
        public Date evdte;
        @com.google.gson.annotations.SerializedName("type")
        public String typ;
        @com.google.gson.annotations.SerializedName("title")
        public String title;
        @com.google.gson.annotations.SerializedName("subject_Id")
        public String Subid;
        @com.google.gson.annotations.SerializedName("target_Name")
        public String name;
        @com.google.gson.annotations.SerializedName("mark")
        public String mark;

        @com.google.gson.annotations.SerializedName("teacherName")
        public String Teacher;
        @com.google.gson.annotations.SerializedName("division_Id")
        public String divid;
        @com.google.gson.annotations.SerializedName("day")
        public String Day;
        @com.google.gson.annotations.SerializedName("completed")
        public Boolean status;
        public Boolean statusChecked = false;
        public Integer position;

        public String getId() {
            return mId;
        }
    }

}
