package in.gravitykerala.user.staff_mschool;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import in.gravitykerala.user.staff_mschool.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
