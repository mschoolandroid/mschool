package in.gravitykerala.user.staff_mschool;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by USER on 9/19/2015.
 */
public class StaffStudentIndividualDTO {
    @com.google.gson.annotations.SerializedName("id")
    public String mId;
    ArrayList<Single> std;

    StaffStudentIndividualDTO() {
        std = new ArrayList<Single>();

    }


    public class Single {
        @com.google.gson.annotations.SerializedName("id")
        public String mId;

        @com.google.gson.annotations.SerializedName("status")
        public Boolean status;
        @com.google.gson.annotations.SerializedName("event_Date")
        public Date evdate;

        @com.google.gson.annotations.SerializedName("student_Name")
        public String studnam;


    }

}


