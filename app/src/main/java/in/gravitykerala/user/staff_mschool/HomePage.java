package in.gravitykerala.user.staff_mschool;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import in.gravitykerala.user.staff_mschool.R;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.microsoft.windowsazure.notifications.NotificationsManager;

import java.util.AbstractList;
import java.util.List;

public class HomePage extends AppCompatActivity {
    Button attendancemark, attendanceview, notificationsender;
    TextView tvname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        AzureMobileService.initialize(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setIcon(R.mipmap.mschool_icon_status_bar);
        }


        Button attendancemark = (Button) findViewById(R.id.button_markattendance);
        Button profileview = (Button) findViewById(R.id.button3);
        Button markleave = (Button) findViewById(R.id.button5);
        Button noti = (Button) findViewById(R.id.button6);
        Button notificationsender = (Button) findViewById(R.id.button_notification_sender);
        Button viewleave = (Button) findViewById(R.id.viewleave);
        tvname = (TextView) findViewById(R.id.textView4);
        // attendanceview.setVisibility(attendanceview.GONE);
//        LinearLayout mark_layout = (LinearLayout) findViewById(R.id.layout_marking);
//        LinearLayout view_layout = (LinearLayout) findViewById(R.id.layout_viewing);
//        TextView view = (TextView) findViewById(R.id.text_view);
        attendancemark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomePage.this, Attendance_marking.class);
                startActivity(i);
            }
        });
        profileview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomePage.this, ProfileStudents.class);
                startActivity(i);
            }
        });

        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomePage.this, Notification.class);
                startActivity(i);
            }
        });
        notificationsender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomePage.this, SendTask.class);
                startActivity(i);
            }
        });
        viewleave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomePage.this, ViewMarkedleave.class);
                startActivity(i);

            }
        });
        markleave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomePage.this, MarkLeave.class);
                startActivity(i);

            }
        });
        refreshItemsFromTable();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_page, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.change_pwd) {
            Intent i = new Intent(HomePage.this, ChangePasswordActivity.class);
            startActivity(i);
        } else if (id == R.id.send_notification) {
            Intent i = new Intent(HomePage.this, SendNotification.class);
            startActivity(i);
        } else if (id == R.id.developer_info) {
            Intent i = new Intent(HomePage.this, AboutPage.class);
            startActivity(i);
        } else if (id == R.id.send_task) {
            Intent i = new Intent(HomePage.this, SendTask.class);
            startActivity(i);
        } else if (id == R.id.logout) {
            AzureMobileService.clearUserToken(this);

        }
        return super.onOptionsItemSelected(item);
    }

    public void refreshItemsFromTable() {

        // Get the items that weren't marked as completed and add them in the
        // adapter


//        mProgressBar.setVisibility(ProgressBar.VISIBLE);
        List<Pair<String, String>> parameters = new AbstractList<Pair<String, String>>() {
            @Override
            public Pair<String, String> get(int i) {
                return null;
            }

            @Override
            public int size() {
                return 0;
            }
        };
        // String id = getIntent().getExtras().getString("id");
        ListenableFuture<StaffProfileDTO> result = AzureMobileService.client.invokeApi("StaffProfileDTO", StaffProfileDTO.class);

        Futures.addCallback(result, new FutureCallback<StaffProfileDTO>() {
            @Override
            public void onFailure(Throwable exc) {
                exc.printStackTrace();
                Log.d("Output", "error");
                //createAndShowDialog((Exception) exc, "Error");
            }

            @Override
            public void onSuccess(StaffProfileDTO result) {
                if (result != null) {
//                    table.setVisibility(TableLayout.VISIBLE);
//                    mProgressBar.setVisibility(ProgressBar.GONE);

                    tvname.setText(result.name);
                    PushNotificationHandler.storePushNotificationTags(result, HomePage.this);
                } else {

                }
                Log.d("PushNotification:", "registering");
                NotificationsManager.handleNotifications(HomePage.this, GravitySupport.GCM_PUSH_SENDER_ID, PushNotificationHandler.class);
                Log.d("PushNotification:", "registered");
            }
        });


    }

}
