package in.gravitykerala.user.staff_mschool;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by USER on 12/10/2015.
 */
public class StaffDivisionAttendance {
    @com.google.gson.annotations.SerializedName("id")
    public String mId;
    ArrayList<Single> std;

    StaffDivisionAttendance() {
        std = new ArrayList<Single>();

    }


    public class Single {
        @com.google.gson.annotations.SerializedName("id")
        public String mId;

        @com.google.gson.annotations.SerializedName("division_Name")
        public Boolean divname;

        @com.google.gson.annotations.SerializedName("division_Id")
        public String divid;

        @com.google.gson.annotations.SerializedName("subject_Id")
        public String subid;

        @com.google.gson.annotations.SerializedName("subject_Name")
        public Boolean subname;

        @com.google.gson.annotations.SerializedName("strength")
        public int str;

        @com.google.gson.annotations.SerializedName("absent")
        public int absentnum;

        @com.google.gson.annotations.SerializedName("present")
        public int prenum;

        @com.google.gson.annotations.SerializedName("event_Date")
        public Date evdate;


    }


}
