package in.gravitykerala.user.staff_mschool;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

import in.gravitykerala.user.staff_mschool.R;

public class SuccessDailogTaskAssign extends Dialog {
    public SuccessDailogTaskAssign(final Context context) {
        super(context);
        setCancelable(false);
        setContentView(R.layout.activity_success_dailog_task_assign);
        getWindow().setBackgroundDrawableResource(R.color.primary_light);
        setTitle("mSchool");
        show();
        Button btn = (Button) findViewById(R.id.button9);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, HomePage.class);
                context.startActivity(i);
                ((Activity) context).finish();

            }

        });
    }
}