package in.gravitykerala.user.staff_mschool;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
//import com.microsoft.windowsazure.notifications.NotificationsManager;

import java.util.AbstractList;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileView extends AppCompatActivity implements GravitySupport {
    private static final String KEY_TITLE = "title";
    //  public static MobileServiceClient mClient;
    ImageButton attendnce, reslt, othr;
    MobileServiceTable<StudentProfile> mToDoTable;
    MobileServiceTable<Remarks> mremarks;
    //    private TextView tv7;
    ProgressBar mProgressBar;
    EditText remarks;
    Button sendremarks;
    LinearLayout table;
    private TextView tv;
    private TextView tv1;
    private TextView tv2;
    private TextView tv3;
    private TextView tv4;
    private TextView tv5;
    private TextView tv6;

    public ProfileView() {

        // Required empty public constructor
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar_profile_view);
        mProgressBar.setVisibility(ProgressBar.VISIBLE);
//        ShapeDrawable drawable = new ShapeDrawable(new OvalShape());
//        drawable.getPaint().setColor(getResources().getColor(R.color.white));
        remarks = (EditText) findViewById(R.id.remarks);
        table = (LinearLayout) findViewById(R.id.tr);


//        setUI(false, true);
        AzureMobileService.initialize(this);
        mToDoTable = AzureMobileService.client.getTable(StudentProfile.class);
        mremarks = AzureMobileService.client.getTable(Remarks.class);

        tv = (TextView) findViewById(R.id.name_tv);
        tv1 = (TextView) findViewById(R.id.phn_tv);
        tv2 = (TextView) findViewById(R.id.bloodgroup_tv);
        tv3 = (TextView) findViewById(R.id.difficulties_tv);
        tv4 = (TextView) findViewById(R.id.reg_no_tv);
        tv5 = (TextView) findViewById(R.id.username_tv);
        tv6 = (TextView) findViewById(R.id.standard_tv);
        sendremarks = (Button) findViewById(R.id.button_remarks);

//        tv7 = (TextView) view.findViewById(R.id.textView17);
        isOnline();
        refreshItemsFromTable();
//        table.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ProfileView.this, "Enter your remarks about " + tv.getText().toString(), Toast.LENGTH_SHORT).show();
                remarks.setVisibility(view.VISIBLE);
                sendremarks.setVisibility(view.VISIBLE);
                fab.setVisibility(FloatingActionButton.GONE);

            }
        });

        sendremarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                remarks.setVisibility(EditText.VISIBLE);

                String remarks_str = remarks.getText().toString();
                if (remarks_str == "") {
                    Toast.makeText(ProfileView.this, "Enter your Remarks", Toast.LENGTH_SHORT).show();

                } else {
                    submit();
                    Toast.makeText(ProfileView.this, "Sending Remarks", Toast.LENGTH_SHORT).show();
                    remarks.setText("");
                }
            }
        });


    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            Toast.makeText(ProfileView.this, "No Data Connection", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    public void refreshItemsFromTable() {

        // Get the items that weren't marked as completed and add them in the
        // adapter
        setUI(false, true);

//        mProgressBar.setVisibility(ProgressBar.VISIBLE);
        List<Pair<String, String>> parameters = new AbstractList<Pair<String, String>>() {
            @Override
            public Pair<String, String> get(int i) {
                return null;
            }

            @Override
            public int size() {
                return 0;
            }
        };
        String id = getIntent().getExtras().getString("id");
        Profilestdid req = new Profilestdid();
        req.studentId = id;
        ListenableFuture<StudentProfile> result = AzureMobileService.client.invokeApi("StudentProfileDTO", req, StudentProfile.class);

        Futures.addCallback(result, new FutureCallback<StudentProfile>() {
            @Override
            public void onFailure(Throwable exc) {
                exc.printStackTrace();
                Log.d("Output", "error");
                //createAndShowDialog((Exception) exc, "Error");
            }

            @Override
            public void onSuccess(StudentProfile result) {
                if (result != null) {
//                    table.setVisibility(TableLayout.VISIBLE);
//                    mProgressBar.setVisibility(ProgressBar.GONE);
                    setUI(true, false);
                    tv.setText(result.getStudentName());
                    tv1.setText(result.contact);
                    tv2.setText(result.bloodgroup);
                    tv3.setText(result.diffcuties);
                    tv4.setText(result.getRegno());
                    tv5.setText(result.getUsername());
                    tv6.setText(result.getDepartment());
                } else {

                }

            }
        });


    }

    private void setUI(Boolean tableLayoutVisible,
                       Boolean progressVisible) {


        if (table != null) {   //Check that UI is assigned
            if (tableLayoutVisible) {
                table.setVisibility(LinearLayout.VISIBLE);
            } else {
                table.setVisibility(LinearLayout.GONE);
            }
        }


        if (mProgressBar != null) {   //Check that UI is assigned
            if (progressVisible) {
                mProgressBar.setVisibility(LinearLayout.VISIBLE);
            } else {
                mProgressBar.setVisibility(LinearLayout.GONE);
            }
        }

    }

    public void submit() {


        if (AzureMobileService.client == null) {
            return;
        }
        String id = getIntent().getExtras().getString("id");
        // Create a new item
        final Remarks item = new Remarks();
        item.sId = id;
        item.Dsc = (remarks.getText().toString());
        item.evdte = new Date();

        // item.setComplete(false);

        // Insert the new item

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final Remarks entity = mremarks.insert(item).get();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileView.this);
                            alertDialogBuilder.setTitle("Sent a remark");


                            alertDialogBuilder
                                    .setMessage("Remark successfully sent")
                                    .setCancelable(false)

//                                    .setPositiveButton("Yes",
//                                            new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    moveTaskToBack(false);
//                                                    submit();
//                                                }
//                                            })

                                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                            remarks.setText("");

                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
//                    createAndShowDialog(e, "Error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileView.this);
                            alertDialogBuilder.setTitle("Error!");


                            alertDialogBuilder
                                    .setMessage("Remark sending failed. Check your internet connectivity")
                                    .setCancelable(false)

//                                    .setPositiveButton("Yes",
//                                            new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    moveTaskToBack(false);
//                                                    submit();
//                                                }
//                                            })

                                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alertDialog1 = alertDialogBuilder.create();
                            alertDialog1.show();
                            //setUI(false, true, true, true, true);

                        }
                    });
                }
//                setUI(false,true,true,true,true);
                return null;
            }

        }.execute();


    }

    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if (exception.getCause() != null) {
            ex = exception.getCause();
        }
        // createAndShowDialog(ex.getMessage(), title);
    }
}
