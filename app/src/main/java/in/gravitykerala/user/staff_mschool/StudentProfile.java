package in.gravitykerala.user.staff_mschool;


import com.google.gson.annotations.SerializedName;

public class StudentProfile {


    @SerializedName("emergencyContact")
    public String contact;
    @SerializedName("bloodgroup")
    public String bloodgroup;
    @SerializedName("difficulties")
    public String diffcuties;
    /**
     * student_Name : sample string 1
     * course : sample string 2
     * course_Id : sample string 3
     * classRoom_Id : sample string 4
     * classRoom : sample string 5
     * branch : sample string 6
     * branch_Id : sample string 7
     * department : sample string 8
     * department_Id : sample string 9
     * semester : sample string 10
     * semester_Id : sample string 11
     * regno : sample string 12
     * username : sample string 13
     * id : sample string 14
     * __version : QEBA
     * __createdAt : 2015-11-12T05:36:10.018Z
     * __updatedAt : 2015-11-12T05:36:10.018Z
     * __deleted : true
     */

    @SerializedName("student_Name")
    private String studentName;
    @SerializedName("course")
    private String course;
    @SerializedName("course_Id")
    private String courseId;
    @SerializedName("classRoom_Id")
    private String classRoomId;
    @SerializedName("classRoom")
    private String classRoom;
    @SerializedName("branch")
    private String branch;
    @SerializedName("branch_Id")
    private String branchId;
    @SerializedName("standard")
    private String department;
    @SerializedName("standard_Id")
    private String departmentId;
    @SerializedName("semester")
    private String semester;
    @SerializedName("semester_Id")
    private String semesterId;
    @SerializedName("regno")
    private String regno;
    @SerializedName("username")
    private String username;
    @SerializedName("id")
    private String id;

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getSemesterId() {
        return semesterId;
    }

    public void setSemesterId(String semesterId) {
        this.semesterId = semesterId;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}