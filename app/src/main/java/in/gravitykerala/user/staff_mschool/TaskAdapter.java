package in.gravitykerala.user.staff_mschool;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import java.util.List;

/**
 * Adapter to bind a ToDoItem List to a view
 */
public class TaskAdapter extends ArrayAdapter<StudentTask.Exam> {

    public List<StudentTask.Exam> dataList;
    /**
     * Adapter context
     */
    Context mContext;


    /**
     * Adapter View layout
     */
    int mLayoutResourceId;

    public TaskAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mContext = context;
        mLayoutResourceId = layoutResourceId;
    }

    /**
     * Returns the view for a specific item on the list
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;

        final StudentTask.Exam currentItem = getItem(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(mLayoutResourceId, parent, false);
        }
        final CheckBox checkBox = (CheckBox) row.findViewById(R.id.checkBox1);
        checkBox.setText("complete");
        checkBox.setChecked(currentItem.status);
        currentItem.position = position;
        dataList.set(position, currentItem);
        row.setTag(currentItem);
        String status;
        if (currentItem.status == true) {
            status = "completed";

        } else {
            status = "Not completed";
        }
//        final TextView tvTitle = (TextView) row.findViewById(R.id.subname);
        final TextView tvContent = (TextView) row.findViewById(R.id.Teacher);
//        final TextView tvContent0 = (TextView) row.findViewById(R.id.Day);
//        final TextView tvContent1 = (TextView) row.findViewById(R.id.Present);
//        tvTitle.setText("SUBJECT" + ":" + "\t" + currentItem.Subname);
        final EditText mark = (EditText) row.findViewById(R.id.editText);
        final String m = mark.getText().toString();
//        tvTitle.setText("SUBJECT" + ":" + "\t" + currentItem.Subname);

//        tvContent0.setText("Due Date" + ":" + currentItem.evdte.getDate()+"-"+currentItem.evdte.getMonth()+"-"+currentItem.evdte.getYear()+1900);
//        tvContent0.setTextColor(Color.RED);
        tvContent.setText(currentItem.name);
//        tvContent1.setText("STATUS" + ":" + "\t" + status);

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (m == null) {
                    Toast.makeText(mContext, "Please enter mark", Toast.LENGTH_SHORT).show();
                } else {
                    if (checkBox.isChecked()) {
                        currentItem.statusChecked = true;
                        if (mContext instanceof ViewTask) {
                            ViewTask activity = (ViewTask) mContext;
                            currentItem.mark = mark.getText().toString();

                            activity.checkItem(currentItem);

                        }

                    } else if (!checkBox.isChecked()) {
                        currentItem.statusChecked = false;
                        if (mContext instanceof ViewTask) {
                            ViewTask activity = (ViewTask) mContext;
                            activity.uncheckItem(currentItem);
                        }
                    }
                }
                dataList.set(position, currentItem);
            }
        });

        return row;
    }
}
