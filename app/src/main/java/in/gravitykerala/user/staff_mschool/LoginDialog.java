package in.gravitykerala.user.staff_mschool;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.microsoft.windowsazure.mobileservices.ApiOperationCallback;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.authentication.MobileServiceUser;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;

/**
 * TODO: document your custom view class.
 */
public class LoginDialog extends Dialog {
    EditText email, password;
    Button login, forget_password;
    LinearLayout interactiveLayout;
    ProgressBar progressBar;
    MobileServiceClient mClientLogin = null;


    public LoginDialog(final Context context) {
        super(context);
        Log.d("LoginDialog:", "Started");
        setContentView(R.layout.login_dialog);
        getWindow().setBackgroundDrawableResource(R.color.primary_light
        );
        setCancelable(false);
        email = (EditText) findViewById(R.id.editText_login_email);
        password = (EditText) findViewById(R.id.editText_login_password);
        login = (Button) findViewById(R.id.button_login);
        // forget_password = (Button) findViewById(R.id.button2);
        progressBar = (ProgressBar) findViewById(R.id.progressBar_login);
        interactiveLayout = (LinearLayout) findViewById(R.id.layout_interactive);
        show();
        setTitle("mSchool");

//        forget_password.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(context, ForgetPasswordActivity.class);
//                context.startActivity(i);
//            }
//        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("LoginDialog:", "Clicked");
                disableUI();
                LoginRequest req = new LoginRequest();
                req.uName = email.getText().toString();
                req.Pword = password.getText().toString();
//                Log.d("LoginCred", "Username:" + req.uName + "Password:" + req.Pword);


                try {
                    mClientLogin = new MobileServiceClient(AzureMobileService.API_URL,
                            AzureMobileService.API_KEY,
                            context);
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                SplashActivity.initializeMclient(context);
                mClientLogin.invokeApi("CustomLogin", req, CustomLoginResult.class, new ApiOperationCallback<CustomLoginResult>() {
                    //TODO convert to non-deprecated function; remember about Synchronization
                    @Override
                    public void onCompleted(CustomLoginResult result, Exception exception, ServiceFilterResponse response) {
                        synchronized (AzureMobileService.mAuthenticationLock) {
                            if (exception == null) {
                                Log.d("LoginDialog:", "Success");
                                String userId = result.uId.replace("CUSTOM:", "");
                                MobileServiceUser user = new MobileServiceUser(userId);
                                String tok = result.Mtoken;
                                user.setAuthenticationToken(tok);
                                AzureMobileService.client.setCurrentUser(user);
                                AzureMobileService.cacheUserToken(user, context);

//                                Log.d("Result_SUCCESS_UID", userId);
//                                Log.d("Result_SUCCESS_Token", tok);

//                                AzureMobileService.bAuthenticating = false;
//                                AzureMobileService.mAuthenticationLock.notifyAll();

//                            Intent intent = new Intent(getApplicationContext(), Attendance_marking.class);
//                            startActivity(intent);
//                            finish();
                                AzureMobileService.bAuthenticating = false;
                                AzureMobileService.mAuthenticationLock.notifyAll();
                                if (context instanceof SplashActivity) {
                                    Intent i = new Intent(context, HomePage.class);
                                    context.startActivity(i);
                                    ((SplashActivity) context).finish();
                                }
                                dismiss();

                            } else {
//                            onLoginFailed();
                                exception.printStackTrace();
                                Log.d("LoginDialog:", "Failed");
                                enableUI();
                                Toast.makeText(context, "Login failed!" + result, Toast.LENGTH_SHORT).show();
                                exception.printStackTrace();
                                enableUI();
                            }

                        }
                    }
                });

            }
        });

    }

    private void enableUI() {
        interactiveLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

    }

    private void disableUI() {
        interactiveLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

    }
}
