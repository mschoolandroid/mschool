package in.gravitykerala.user.staff_mschool;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.gravitykerala.user.staff_mschool.R;

/**
 * Adapter to bind a ToDoItem List to a view
 */
public class ListAdapterStudents extends ArrayAdapter<StaffDivisionStudents> {

    /**
     * Adapter context
     */
    Context mContext;


    /**
     * Adapter View layout
     */
    int mLayoutResourceId;

    public ListAdapterStudents(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mContext = context;
        mLayoutResourceId = layoutResourceId;
    }

    /**
     * Returns the view for a specific item on the list
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        final StaffDivisionStudents currentItem = getItem(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(mLayoutResourceId, parent, false);
        }

        row.setTag(currentItem);
        final LinearLayout lv = (LinearLayout) row.findViewById(R.id.lv1);
        final TextView tv = (TextView) row.findViewById(R.id.textView);
        final TextView tv1 = (TextView) row.findViewById(R.id.textView3);
        tv.setText("" + currentItem.rolno);
        tv1.setText("" + currentItem.StudentName);
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                StaffDivisionStudents clickedStudent = (StaffDivisionStudents) v.getTag();
                Intent newActivity0 = new Intent(mContext, StudentsingleSelect.class);
                newActivity0.putExtra("id", clickedStudent.mId);
                newActivity0.putExtra("divsionid", clickedStudent.DivId);
                newActivity0.putExtra("subid", StudentwiseAttFragment.selectsubId);
                newActivity0.putExtra("name", clickedStudent.StudentName);

                Log.d("id", clickedStudent.mId);
                mContext.startActivity(newActivity0);


            }
        });

        return row;
    }

}
