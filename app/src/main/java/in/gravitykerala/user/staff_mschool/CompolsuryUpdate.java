package in.gravitykerala.user.staff_mschool;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;

import in.gravitykerala.user.staff_mschool.BuildConfig;
import in.gravitykerala.user.staff_mschool.R;

public class CompolsuryUpdate extends Dialog {
    public CompolsuryUpdate(final Context context) {
        super(context);
        setCancelable(false);
        setContentView(R.layout.activity_compolsury_update);
        getWindow().setBackgroundDrawableResource(R.color.primary_light);
        setTitle("mCampus");
        show();
        Button btn = (Button) findViewById(R.id.button_playstore);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID));
                context.startActivity(browserIntent);
            }
        });
    }
}