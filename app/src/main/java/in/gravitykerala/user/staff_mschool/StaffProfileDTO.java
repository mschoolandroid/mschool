package in.gravitykerala.user.staff_mschool;

import com.google.gson.annotations.SerializedName;

/**
 * Created by USER on 1/11/2016.
 */
public class StaffProfileDTO {
    @SerializedName("staff_Name")
    public String name;
    @SerializedName("department")
    public String dep;
    @SerializedName("department_Id")
    public String depid;
    @SerializedName("id")
    public String mid;
    @SerializedName("username")
    public String uname;
}
