package in.gravitykerala.user.staff_mschool;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.microsoft.windowsazure.mobileservices.MobileServiceException;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOrder;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class StudentwiseAttFragment extends Fragment {
    public static String selectsubId = "none";
    Spinner spinnerDivision, spinnerSubject, spinnerDay;
    ListView studentsListview;
    LinearLayout class_layout, subject_layout;
    Context currentContext;
    AsyncSubjectLoader SubAsyncTask;
    AsyncDivisionLoader DivAsyncTask;
    String selectedDivId = "none";
    private ListAdapterStudents studentsAdapter;
    private ProgressBar mProgressBar;
    // private ProgressBar mProgressBar1;
    private MobileServiceTable<StaffDivision> StaffDivTable;
    private MobileServiceTable<StaffDivisionStudents> StudentsTable;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_stdntwise, container, false);
        currentContext = getActivity();
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar1);
        // mProgressBar1 = (ProgressBar) rootView.findViewById(R.id.progressBar2);
//        mProgressBar.setVisibility(ProgressBar.GONE);
        setUI(true, false, false, false);
        // mProgressBar1.setVisibility(ProgressBar.GONE);
        class_layout = (LinearLayout) rootView.findViewById(R.id.class_layout);
        subject_layout = (LinearLayout) rootView.findViewById(R.id.subject_layout);

        spinnerDivision = (Spinner) rootView.findViewById(R.id.spinner);
        spinnerSubject = (Spinner) rootView.findViewById(R.id.spinner2);
        studentsAdapter = new ListAdapterStudents(getActivity(), R.layout.list_single);
        studentsListview = (ListView) rootView.findViewById(R.id.listView);
        studentsListview.setAdapter((ListAdapter) studentsAdapter);
        AzureMobileService.initialize(getActivity());
//        SplashActivity.Storetok(getActivity());
//        SplashActivity.initializeMclient(getActivity());
        // SplashActivity.Storetok(getActivity());
        initializeTables();
        refreshDivisionFromTable();
        spinnerDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                if (position == 0) {
//                    setUI(false,true,false,false);
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(getActivity(), "Select any class", Toast.LENGTH_LONG).show();
                } else {
                    selectedDivId = (String) arg1.getTag();
//                    setUI(false,true,true,false);
//                    Toast.makeText(getActivity(), selectedDivId, Toast.LENGTH_LONG).show();
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, resultStringList);
                    refreshSubjectFromTable();
                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                if (position == 0) {
//                    setUI(false,true,true,false);

                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(getActivity(), "Select any subject", Toast.LENGTH_LONG).show();
                } else {

                    selectsubId = (String) arg1.getTag();
//                    setUI(false,true,true,true);
//                    Toast.makeText(getActivity(), selectsubId, Toast.LENGTH_LONG).show();
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, resultStringList);
                    getStudentlist();
                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        return rootView;
    }

    private void setUI(Boolean progressVisible, Boolean classLayoutVisible, Boolean subjectLayoutVisible, Boolean studentListVisible) {
        if (mProgressBar != null) {   //Check that UI is assigned
            if (progressVisible) {
                mProgressBar.setVisibility(LinearLayout.VISIBLE);
            } else {
                mProgressBar.setVisibility(LinearLayout.GONE);
            }
        }


        if (class_layout != null) {   //Check that UI is assigned
            if (classLayoutVisible) {
                class_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                class_layout.setVisibility(LinearLayout.GONE);
            }
        }
        if (subject_layout != null) {   //Check that UI is assigned
            if (subjectLayoutVisible) {
                subject_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                subject_layout.setVisibility(LinearLayout.GONE);
            }
        }
        if (studentsListview != null) {   //Check that UI is assigned
            if (studentListVisible) {
                studentsListview.setVisibility(LinearLayout.VISIBLE);
            } else {
                studentsListview.setVisibility(LinearLayout.GONE);
            }
        }
    }

    public void subdivids() {
        String subid = selectsubId;
        String divid = selectedDivId;
    }


    public void refreshSubjectFromTable() {
        SubAsyncTask = new AsyncSubjectLoader();
        SubAsyncTask.execute();
    }

    public void refreshDivisionFromTable() {
        DivAsyncTask = new AsyncDivisionLoader();
        DivAsyncTask.execute();
    }

    public void getStudentlist() {
        mProgressBar.setVisibility(ProgressBar.VISIBLE);
//        mProgressBar.setVisibility(ProgressBar.VISIBLE);
//        setUI(true,true,true,true);
        // Get the items that weren't marked as completed and add them in the
        // adapter

        new AsyncTask<Void, Void, Void>() {

            @Override

            protected Void doInBackground(Void... params) {

                try {

                    final List<StaffDivisionStudents> results =
                            StudentsTable.where().field("Division_Id").eq(selectedDivId).orderBy("roll_No", QueryOrder.Ascending).execute().get();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // mSwipeLayout.setRefreshing(false);
                            if (results.size() == 0) {
                                Toast.makeText(getActivity(), "No students", Toast.LENGTH_LONG).show();
                                mProgressBar.setVisibility(ProgressBar.GONE);
//                                setUI(false,false,false,false);
                            } else {
                                for (StaffDivisionStudents item : results) {
                                    mProgressBar.setVisibility(ProgressBar.GONE);

                                    studentsAdapter.add(item);
                                    // pull.setVisibility(View.GONE);
                                }


                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();


                    // createAndShowDialog(e, "Error");


                }
                return null;
            }

            protected void onPostExecute(Void results) {
                super.onPostExecute(results);
//
            }
        }.execute();

        studentsAdapter.clear();
    }

    private void initializeTables() {
        try {
            StaffDivTable = AzureMobileService.client.getTable("StaffDivision", StaffDivision.class);
            StudentsTable = AzureMobileService.client.getTable("StaffDivisionStudents", StaffDivisionStudents.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroyView();

        try {
            if (SubAsyncTask != null)
                SubAsyncTask.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (DivAsyncTask != null)
                DivAsyncTask.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private class AsyncSubjectLoader extends AsyncTask<Void, Void, List<StaffDivision>> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            setUI(true, true, false, false);
        }

        @Override
        protected List<StaffDivision> doInBackground(Void... params) {
            List<StaffDivision> results = null;
            try {
                results = StaffDivTable.where().field("Division_Id").eq(selectedDivId).execute().get();

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<StaffDivision> courseDTOs) {
            super.onCancelled(courseDTOs);
            mProgressBar.setVisibility(ProgressBar.GONE);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            mProgressBar.setVisibility(ProgressBar.GONE);
        }

        @Override
        protected void onPostExecute(List<StaffDivision> departmentItemDTOs) {
            super.onPostExecute(departmentItemDTOs);

            if (departmentItemDTOs != null && !departmentItemDTOs.isEmpty()) //Successfull results
            {
                StaffDivision dummySelector = new StaffDivision(); //Used for showing the first element to be a prompt
                dummySelector.Subname = "Select your Subject";
                // dummySelector.courseId = "none";
                departmentItemDTOs.add(0, dummySelector);

                StaffDivAdapter ClssAdapter = new StaffDivAdapter(getActivity(), R.layout.spinner_selector, R.layout.spinner_dropdown, departmentItemDTOs);
                spinnerSubject.setAdapter(ClssAdapter);
//                sp_Department.setSelection(-1);

                // Toast.makeText(getBaseContext(), "Please Select your Course", Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(getActivity(), "Error retrieving courses, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }
            setUI(false, true, true, true);


        }
    }

    private class AsyncDivisionLoader extends AsyncTask<Void, Void, List<StaffDivision>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            setUI(true, false, false, false);
        }

        @Override
        protected List<StaffDivision> doInBackground(Void... params) {
            List<StaffDivision> results = null;
            try {
                try {
                    results = StaffDivTable.execute().get();
                } catch (MobileServiceException e) {
                    e.printStackTrace();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<StaffDivision> courseDTOs) {
            super.onCancelled(courseDTOs);
            setUI(false, true, true, true);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            setUI(false, true, true, true);
        }

        @Override
        protected void onPostExecute(List<StaffDivision> ClassitemDTO) {
            super.onPostExecute(ClassitemDTO);

            if (ClassitemDTO != null && !ClassitemDTO.isEmpty()) //Successfull results
            {
                StaffDivision dummySelector = new StaffDivision(); //Used for showing the first element to be a prompt
                dummySelector.DivName = "Select your Class";
                dummySelector.Divid = "none";
                ClassitemDTO.add(0, dummySelector);

                DivAdapter depAdapter = new DivAdapter(getActivity(), R.layout.spinner_selector, R.layout.spinner_dropdown, ClassitemDTO);
                spinnerDivision.setAdapter(depAdapter);
//                sp_Department.setSelection(-1);


            } else {
                Toast.makeText(getActivity(), "Error retrieving courses, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }
            setUI(false, true, false, false);
        }
    }
}
