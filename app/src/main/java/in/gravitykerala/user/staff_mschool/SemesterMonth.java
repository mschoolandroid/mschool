package in.gravitykerala.user.staff_mschool;

import java.util.Date;

/**
 * Created by USER on 9/9/2015.
 */
public class SemesterMonth {


    @com.google.gson.annotations.SerializedName("id")
    public String mId;

    @com.google.gson.annotations.SerializedName("month_Name")
    public String mname;

    @com.google.gson.annotations.SerializedName("month_Date")
    public String mdate;
    @com.google.gson.annotations.SerializedName("semesterName")
    public String msemname;
    @com.google.gson.annotations.SerializedName("event_Date")
    public Date evdate;

    @com.google.gson.annotations.SerializedName("month_Number")
    public String mnum;
    @com.google.gson.annotations.SerializedName("semester_Id")
    public String msid;


}
