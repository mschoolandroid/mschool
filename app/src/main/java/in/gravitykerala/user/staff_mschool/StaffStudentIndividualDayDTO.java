package in.gravitykerala.user.staff_mschool;

import java.util.ArrayList;

/**
 * Created by USER on 12/7/2015.
 */
public class StaffStudentIndividualDayDTO {
    @com.google.gson.annotations.SerializedName("id")
    public String mId;
    ArrayList<Single> std;

    StaffStudentIndividualDayDTO() {
        std = new ArrayList<Single>();

    }


    public class Single {
        @com.google.gson.annotations.SerializedName("id")
        public String mId;

        @com.google.gson.annotations.SerializedName("status")
        public Boolean status;

        @com.google.gson.annotations.SerializedName("student_Name")
        public String studnam;

        @com.google.gson.annotations.SerializedName("day")
        public String day;


    }


}
