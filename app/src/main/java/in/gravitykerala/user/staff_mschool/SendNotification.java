package in.gravitykerala.user.staff_mschool;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.microsoft.windowsazure.mobileservices.MobileServiceException;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class SendNotification extends AppCompatActivity {
    Spinner spinnerDivision;
    MobileServiceTable<StudentNotification> mToDoTable;
    LinearLayout class_layout, subject_layout;

    Context currentContext;

    AsyncDivisionLoader DivAsyncTask;
    String selectsubId = "none";
    String selectedDivId = "none";
    EditText title, content;
    Button submit_btn;
    private ListAdapterStudents studentsAdapter;
    private ProgressBar mProgressBar;
    // private ProgressBar mProgressBar1;
    private MobileServiceTable<StaffDivision> StaffDivTable;
    private MobileServiceTable<StaffDivisionStudents> StudentsTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_notification);
        currentContext = SendNotification.this;
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar3);
        // mProgressBar1 = (ProgressBar) rootView.findViewById(R.id.progressBar2);
//        mProgressBar.setVisibility(ProgressBar.GONE);
        setUI(true, false, false, false, false);
        // mProgressBar1.setVisibility(ProgressBar.GONE);
        class_layout = (LinearLayout) findViewById(R.id.spinner_layout);

        title = (EditText) findViewById(R.id.title);
        content = (EditText) findViewById(R.id.content);
        submit_btn = (Button) findViewById(R.id.button2);

        spinnerDivision = (Spinner) findViewById(R.id.spinner_ma_clas);

        studentsAdapter = new ListAdapterStudents(this, R.layout.list_single);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        AzureMobileService.initialize(this);
//
        initializeTables();
        refreshDivisionFromTable();
        spinnerDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                if (position == 0) {
                    setUI(false, true, false, false, false);
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
                    Toast.makeText(SendNotification.this, "Select any class", Toast.LENGTH_LONG).show();
                } else {
                    selectedDivId = (String) arg1.getTag();
                    setUI(false, true, true, true, true);
                    Toast.makeText(SendNotification.this, "Enter Notification Title and content", Toast.LENGTH_LONG).show();
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, resultStringList);

                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (title.getText().toString().equals("") || content.getText().toString().equals("")) {
                    Snackbar.make(view, "Enter all fields", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SendNotification.this);
                    alertDialogBuilder.setTitle("Are you sure want to send notification?");


                    alertDialogBuilder
                            .setMessage("Click yes to send!")
                            .setCancelable(false)

                            .setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            moveTaskToBack(false);
                                            submit();
                                        }
                                    })

                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    dialog.cancel();
                                }
                            });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }
            }
        });

    }

    private void setUI(Boolean progressVisible, Boolean classLayoutVisible, Boolean TitleLayoutVisible,
                       Boolean contentVisible, Boolean submitButton) {
        if (mProgressBar != null) {   //Check that UI is assigned
            if (progressVisible) {
                mProgressBar.setVisibility(LinearLayout.VISIBLE);
            } else {
                mProgressBar.setVisibility(LinearLayout.GONE);
            }
        }


        if (class_layout != null) {   //Check that UI is assigned
            if (classLayoutVisible) {
                class_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                class_layout.setVisibility(LinearLayout.GONE);
            }
        }
        if (title != null) {   //Check that UI is assigned
            if (TitleLayoutVisible) {
                title.setVisibility(LinearLayout.VISIBLE);
            } else {
                title.setVisibility(LinearLayout.GONE);
            }
        }
        if (content != null) {   //Check that UI is assigned
            if (contentVisible) {
                content.setVisibility(LinearLayout.VISIBLE);
            } else {
                content.setVisibility(LinearLayout.GONE);
            }
        }
        if (submit_btn != null) {   //Check that UI is assigned
            if (submitButton) {
                submit_btn.setVisibility(LinearLayout.VISIBLE);
            } else {
                submit_btn.setVisibility(LinearLayout.GONE);
            }
        }


    }

    public void subdivids() {
        String subid = selectsubId;
        String divid = selectedDivId;
    }


    public void refreshDivisionFromTable() {
        DivAsyncTask = new AsyncDivisionLoader();
        DivAsyncTask.execute();
    }


    private void initializeTables() {
        try {
            StaffDivTable = AzureMobileService.client.getTable("StaffDivision", StaffDivision.class);
            StudentsTable = AzureMobileService.client.getTable("StaffDivisionStudents", StaffDivisionStudents.class);
            mToDoTable = AzureMobileService.client.getTable(StudentNotification.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void submit() {


        if (AzureMobileService.client == null) {
            return;
        }

        // Create a new item
        final StudentNotification item = new StudentNotification();
        item.typ = "Class Notification";
        item.title = (title.getText().toString());
        item.evdte = new Date();
        item.cnt = (content.getText().toString());
        item.divid = selectedDivId;
        // item.setComplete(false);

        // Insert the new item
        setUI(true, false, false, false, false);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final StudentNotification entity = mToDoTable.insert(item).get();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SendNotification.this);
                            alertDialogBuilder.setTitle("Notification");


                            alertDialogBuilder
                                    .setMessage("Notification successfully sent")
                                    .setCancelable(false)

//                                    .setPositiveButton("Yes",
//                                            new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    moveTaskToBack(false);
//                                                    submit();
//                                                }
//                                            })

                                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                            title.setText("");
                            content.setText("");
                            setUI(false, true, true, true, true);
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                    createAndShowDialog(e, "Error");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SendNotification.this);
                            alertDialogBuilder.setTitle("Error!");


                            alertDialogBuilder
                                    .setMessage("Notification failed. Check your internet connectivity")
                                    .setCancelable(false)

//                                    .setPositiveButton("Yes",
//                                            new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    moveTaskToBack(false);
//                                                    submit();
//                                                }
//                                            })

                                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alertDialog1 = alertDialogBuilder.create();
                            alertDialog1.show();
                            setUI(false, true, true, true, true);

                        }
                    });
                }
//                setUI(false,true,true,true,true);
                return null;
            }

        }.execute();


    }

    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if (exception.getCause() != null) {
            ex = exception.getCause();
        }
        //createAndShowDialog(ex.toString(), title);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (DivAsyncTask != null)
                DivAsyncTask.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class AsyncDivisionLoader extends AsyncTask<Void, Void, List<StaffDivision>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            setUI(true, false, false, false, false);
        }

        @Override
        protected List<StaffDivision> doInBackground(Void... params) {
            List<StaffDivision> results = null;
            try {
                try {
                    results = StaffDivTable.execute().get();
                } catch (MobileServiceException e) {
                    e.printStackTrace();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<StaffDivision> courseDTOs) {
            super.onCancelled(courseDTOs);
            setUI(false, true, true, true, true);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            setUI(false, true, true, true, true);
        }

        @Override
        protected void onPostExecute(List<StaffDivision> ClassitemDTO) {
            super.onPostExecute(ClassitemDTO);

            if (ClassitemDTO != null && !ClassitemDTO.isEmpty()) //Successfull results
            {
                StaffDivision dummySelector = new StaffDivision(); //Used for showing the first element to be a prompt
                dummySelector.DivName = "Select your Class";
                dummySelector.Divid = "none";
                ClassitemDTO.add(0, dummySelector);

                DivAdapter depAdapter = new DivAdapter(SendNotification.this, R.layout.spinner_selector, R.layout.spinner_dropdown, ClassitemDTO);
                spinnerDivision.setAdapter(depAdapter);
//                sp_Department.setSelection(-1);

                setUI(false, true, false, false, false);
            } else {
                setUI(false, false, false, false, false);
                Toast.makeText(SendNotification.this, "Error retrieving courses, Probably bad network connection!", Toast.LENGTH_LONG).show();

            }

        }
    }
}
