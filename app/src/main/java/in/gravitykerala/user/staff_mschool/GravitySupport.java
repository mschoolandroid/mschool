package in.gravitykerala.user.staff_mschool;


/**
 * Created by Prakash on 18/07/2015.
 */
public interface GravitySupport {


    String KEY_PREFERENCE_ID = "in.gravitykerala.mcampus";
    String DEBUG_API_URL = "https://stjoseph.azurewebsites.net";
    //    String DEBUG_API_URL="http://gravityshared.azurewebsites.net";///https://testmcampus.azure-mobile.net/
    String DEBUG_API_KEY = "Gravity12345"; //GSdkKeAzxCjHbOgajQzfZGUqapBYjp68
    String RELEASE_API_URL = "http://arafaservice.azurewebsites.net/";
    String RELEASE_API_KEY = "arafa12345";

    //    String GCM_PUSH_SENDER_ID_DEBUG = "2145416962"; //Not needed
    String GCM_PUSH_SENDER_ID_RELEASE = "142712835094";
    String GCM_PUSH_SENDER_ID = GCM_PUSH_SENDER_ID_RELEASE;

    String TAG_PREFIX_SEMESTER = "STAFF_SEMESTER:";
    String TAG_PREFIX_CLASSROOM = "STAFF_CLASSROOM:";
    String TAG_PREFIX_DEPARTMENT = "STANDARD_STAFF:";
    String KEY_NOTIFICATION_USER_ID = "NOTIFICATION_USER_ID";
    String KEY_NOTIFICATION_SEMESTER_ID = "NOTIFICATION_SEMESTER_ID";
    String KEY_NOTIFICATION_CLASSROOM_ID = "NOTIFICATION_CLASSROOM_ID";
    String KEY_NOTIFICATION_DEPARTMENT_ID = "NOTIFICATION_STAFFDEPARTMENT_ID";
    String TAG_PREFIX_USER_ID = "UID:";
    String TAG_PREFIX_BRANCH = "STAFF_BRANCH:";
    String KEY_SELECTED_COURSE_ID = "selected_course";
    String KEY_SELECTED_DEPARTMENT_ID = "selected_department";
    String KEY_EXAM_NOTIFICATION_NEEDED = "exam_notification";
    String KEY_DISTANCE_EDUCATION_NEEDED = "distance_status";
    String KEY_VC_DESK_NEEDED = "vc_desk_status";
    String KEY_UOC_NEWS_NEEDED = "uoc_news";
    String KEY_UOC_ORDERS_NEEDED = "uoc_orders";

    String KEY_FIRST_LAUNCH_COURSE_PREF = "course_pref_firstlaunch";
    String KEY_FIRST_LAUNCH_REGISTRATION = "registration_firstlaunch";


    String TAG_NOTIFICATION_COMMON = "NOTIFICATION_UOC_COMMON";//"NOTIFICATION_UOC_COMMON";

//    String CLOUD_SERVICE_URI = "https://universityofcalicut.azure-mobile.net/";
//    String CLOUD_SERVICE_KEY = "XWXXhaCoiYqzzERpfsqnhpJuQBgCAw42";


//      MobileServiceClient mClient = null;

    String TAG_COMMON_DISTANCE = "DISTANCE:"; //Should be put before all distance related tags

    String TAG_COMMON_EXAM = "EXAM:"; //Should be put before all Pareeksha Bhavan related tags

    String TAG_USER_ID = "UID:";

    String TAG_NOTIFICATION_VC_DESK = "NOTIFICATION_UOC_VCDESK";
    String TAG_NOTIFICATION_NEWS = "NOTIFICATION_UOC_NEWS";
    String TAG_NOTIFICATION_ORDERS = "NOTIFICATION_UOC_ORDERS";

    String ID_NOTIFICATION_VC_DESK = "NOTIFICATION_VC_DESK";
    String ID_NOTIFICATION_NEWS = "NOTIFICATION_NEWS";
    String ID_NOTIFICATION_ORDERS = "NOTIFICATION_ORDERS";

    String KEY_NOTIFICATION_TYPE = "NOTIFICATION_TYPE";
    String KEY_NOTIFICATION_TITLE = "NOTIFICATION_TITLE";
    String KEY_NOTIFICATION_CONTENT = "NOTIFICATION_CONTENT";
    String KEY_NOTIFICATION_ID = "NOTIFICATION_ID";
    String KEY_NOTIFICATION_MESSEAGE_TYPE = "MESSEAGE_TYPE";


}