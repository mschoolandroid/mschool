package in.gravitykerala.user.staff_mschool;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by USER on 8/26/2015.
 */
public class AttendanceMarkDTO {
    @SerializedName("id")
    public String mId;

    @SerializedName("subject_Id")
    public String Subid;

    @SerializedName("student_Id")
    public List<String> absenteesList;

    @SerializedName("division_Id")
    public String divid;

    @SerializedName("semester_Id")
    public String semid;

    @SerializedName("day")
    public String day;

    @SerializedName("present")
    public boolean presnt;

    @SerializedName("event_Date")
    public Date evdate;

    @SerializedName("teacher_Id")
    public String teachrid;

    @SerializedName("userId")
    public String Uid;


}

