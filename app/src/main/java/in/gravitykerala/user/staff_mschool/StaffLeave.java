package in.gravitykerala.user.staff_mschool;

import java.util.Date;

/**
 * Created by USER on 7/13/2015.
 */
public class StaffLeave {
    @com.google.gson.annotations.SerializedName("student_Id")
    public String sId;
    @com.google.gson.annotations.SerializedName("tutor_Id")
    public String tId;
    @com.google.gson.annotations.SerializedName("title")
    public String title;
    @com.google.gson.annotations.SerializedName("content")
    public String cnt;
    @com.google.gson.annotations.SerializedName("event_date")
    public Date evdte;
    @com.google.gson.annotations.SerializedName("reviewed")
    public Boolean rvwd;
    @com.google.gson.annotations.SerializedName("approved")
    public Boolean appd;
    @com.google.gson.annotations.SerializedName("id")
    private String mId;

    public String getId() {
        return mId;
    }

    /**
     * Sets the item id
     *
     * @param id id to set
     */
    public final void setId(String id) {
        mId = id;
    }
}
