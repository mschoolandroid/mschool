package in.gravitykerala.user.staff_mschool;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.microsoft.windowsazure.mobileservices.ApiOperationCallback;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;

import java.util.AbstractList;
import java.util.List;


public class ChangePasswordActivity extends AppCompatActivity {
    // public static MobileServiceClient mClient;
    EditText Curerntpwd;
    EditText NewPwd;
    EditText ReNpwd;
    Button Submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
//        SplashActivity.initializeMclient(this);
        // SplashActivity.Storetok(this);
        AzureMobileService.initialize(this);
//        SplashActivity.Storetok(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        Curerntpwd = (EditText) findViewById(R.id.input_crnt_password);
        NewPwd = (EditText) findViewById(R.id.input_new_password);
        ReNpwd = (EditText) findViewById(R.id.input_new_confirm_password);
        Submit = (Button) findViewById(R.id.button);

        //mClient = SplashPage.mClient;
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Npwd = NewPwd.getText().toString();
                String cpwd = ReNpwd.getText().toString();
                if (cpwd.equals(Npwd)) {
                    submit();
                } else {
                    Toast.makeText(ChangePasswordActivity.this, "pasword not match", Toast.LENGTH_LONG).show();

                }
            }
        });
    }


    public void submit() {

        {
            final ProgressDialog progressDialog = new ProgressDialog(ChangePasswordActivity.this,
                    R.style.AppTheme_Dark_Dialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("authenticating");
            progressDialog.show();
            List<Pair<String, String>> parameters = new AbstractList<Pair<String, String>>() {
                @Override
                public Pair<String, String> get(int i) {
                    return null;
                }

                @Override
                public int size() {
                    return 0;
                }
            };
            ChangePassword req = new ChangePassword();
            req.cpwd = Curerntpwd.getText().toString();
            req.npwd = NewPwd.getText().toString();
            String new_pwd_s = NewPwd.getText().toString();
            String rnew_pwd_s = ReNpwd.getText().toString();


            AzureMobileService.client.invokeApi(("ChangePassword"), req, String.class, new ApiOperationCallback<String>() {
                @Override
                public void onCompleted(String result, Exception exception, ServiceFilterResponse response) {
                    if (exception == null) {
                        Toast.makeText(ChangePasswordActivity.this, result, Toast.LENGTH_LONG).show();
                        finish();

                    } else {
                        Toast.makeText(ChangePasswordActivity.this, result, Toast.LENGTH_LONG).show();
                    }
                    progressDialog.dismiss();
                }
            });
        }

    }


}

