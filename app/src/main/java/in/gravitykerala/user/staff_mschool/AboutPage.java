package in.gravitykerala.user.staff_mschool;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import in.gravitykerala.user.staff_mschool.R;

public class AboutPage extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ShapeDrawable drawable = new ShapeDrawable(new OvalShape());
        drawable.getPaint().setColor(getResources().getColor(R.color.white));
        // ((FloatingActionButton) findViewById(R.id.setter_drawable)).setIconDrawable(drawable);

        final com.getbase.floatingactionbutton.FloatingActionButton actionA = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.action_a);
        actionA.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:9746440467"));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                        ) {
                    if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for Activity#requestPermissions for more details.
                        return;
                    }
                }
                startActivity(callIntent);

//                Toast.makeText(getActivity(), "CDEP", Toast.LENGTH_SHORT).show();
//                Intent i= new Intent(getActivity(),LectureNotes.class);
//                startActivity(i);
            }
        });
        final com.getbase.floatingactionbutton.FloatingActionButton actionAB = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.action_ab);
        actionAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "hello@gravityinnovativesolutions.in", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(emailIntent, "Send email..."));

//                Toast.makeText(getActivity(), "KaCS", Toast.LENGTH_SHORT).show();
//
            }
        });
        final com.getbase.floatingactionbutton.FloatingActionButton actionABC = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.action_abcd);
        actionABC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uri = "https://www.google.co.in/maps/place/Gravity+Innovative+Solutions/@10.0532337,76.3256163,14.79z/data=!4m2!3m1!1s0x0:0x9af4116471796d76";
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);


//                Toast.makeText(getActivity(), "CDEP", Toast.LENGTH_SHORT).show();
            }
        });
//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(Intent.ACTION_SEND);
//                intent.setType("plain/text");
//                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"gravitymcampus@gmail.com"});
//                intent.putExtra(Intent.EXTRA_SUBJECT, "mCampus MCET");
//                intent.putExtra(Intent.EXTRA_TEXT, "");
//                startActivity(Intent.createChooser(intent, ""));
//            }
//        });
    }

}
