package in.gravitykerala.user.staff_mschool;

/**
 * Created by USER on 8/25/2015.
 */
public class StaffDivisionStudents {
    @com.google.gson.annotations.SerializedName("id")
    public String mId;

    @com.google.gson.annotations.SerializedName("student_Name")
    public String StudentName;

    @com.google.gson.annotations.SerializedName("roll_No")
    public Integer rolno;

    @com.google.gson.annotations.SerializedName("division_Id")
    public String DivId;

    public Boolean statusChecked = false;
    public Integer position;

    public Integer getrno() {
        return rolno;
    }


}
