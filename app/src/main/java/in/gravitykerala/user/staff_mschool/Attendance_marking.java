package in.gravitykerala.user.staff_mschool;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import in.gravitykerala.user.staff_mschool.R;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.microsoft.windowsazure.mobileservices.MobileServiceException;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOrder;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;


public class Attendance_marking extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    public static ListView gridViewStudents;
    public String date = null;

    public Date seldate = new Date();
    Date d = new Date();
    Spinner spinnerDivision, spinnerSubject, spinnerDay;
    //    public String day = "";
    ListView absenteesListview;
    Button Mark, dateButton;
    TextView tv;
    CheckBox cb, checkAll;
    Context currentContext;
    AsyncSubjectLoader StaffDivAsyncTask;
    AsyncDivisionLoader DivAsyncTask;
    AsyncStudentLoader StudentAsyncTask;
    String selectsubId = "none";
    String selectedDivId = "none";
    String Section = "";
    HashSet<String> absenteesList;
    LinearLayout class_layout, subject_layout, day_layout, li, list_grid;
    private GridAdapterStudents studentsAdapter;
    private ListAdapterAbsentees absenteesAdapter;
    private ProgressBar mProgressBar;

    private ProgressBar mProgressBar1;
    private MobileServiceTable<StaffDivision> StaffDivTable;
    private MobileServiceTable<StaffDivisionStudents> StudentsTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_marking);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        dateButton = (Button) findViewById(R.id.date_button);

//        String dateString = getIntent().getExtras().getString("day");
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        Date convertedDate = new Date();
//        String result = dateString.split(" ")[0];
//
//        try {
//            convertedDate = dateFormat.parse(dateString);
//        } catch (ParseException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }

        tv = (TextView) findViewById(R.id.textView);
        tv.setClickable(true);

//        tv.setText("" + result);

        absenteesList = new HashSet<String>();
        currentContext = this;


        mProgressBar = (ProgressBar) findViewById(R.id.progressBar_ma_small);
        checkAll = (CheckBox) findViewById(R.id.checkAll);
        mProgressBar1 = (ProgressBar) findViewById(R.id.progressBar2);
        mProgressBar.setVisibility(ProgressBar.GONE);
        mProgressBar1.setVisibility(ProgressBar.GONE);
        AzureMobileService.initialize(this);
//        SplashActivity.Storetok(this);
//        StaffDivTable = SplashActivity.mClient.getTable(StaffDivision.class);
//        StudentsTable = SplashActivity.mClient.getTable(StaffDivisionStudents.class);
        spinnerDivision = (Spinner) findViewById(R.id.spinner_ma_class);
        spinnerSubject = (Spinner) findViewById(R.id.spinner_ma_subject);
        spinnerDay = (Spinner) findViewById(R.id.spinner_ma_day);
        li = (LinearLayout) findViewById(R.id.li);
        class_layout = (LinearLayout) findViewById(R.id.class_layout);
        subject_layout = (LinearLayout) findViewById(R.id.subject_layout);
        day_layout = (LinearLayout) findViewById(R.id.day_layout);
//        absentees_layout=(LinearLayout)findViewById(R.id.absentees_layout);
        list_grid = (LinearLayout) findViewById(R.id.layout_list_grid);
        absenteesAdapter = new ListAdapterAbsentees(this, R.layout.list_item);
        absenteesListview = (ListView) findViewById(R.id.listView_ma_absentees);
        absenteesListview.setAdapter(absenteesAdapter);
        studentsAdapter = new GridAdapterStudents(this, R.layout.checkbox_item);
        gridViewStudents = (ListView) findViewById(R.id.gridView_ma_Students);
        gridViewStudents.setAdapter(studentsAdapter);
        cb = (CheckBox) findViewById(R.id.checkAll);
        cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    selectAllFromList(true);
                } else {
                    selectAllFromList(false);
                }

            }
        });
        Mark = (Button) findViewById(R.id.button_ma_students);
        setUI(false, false, false, false, false, false, false, true, false);
        selectsubId = "none";
        selectedDivId = "none";
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();

                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        Attendance_marking.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
//                setUI(false, false, false, false, true, false, false, true);
                dpd.setAccentColor(Color.parseColor("#3F51B5"));
                dpd.show(getFragmentManager(), "Datepickerdialog");


            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(Attendance_marking.this, "Enter your remarks about " + tv.getText().toString(), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Attendance_marking.this, AttendanceView.class);
                startActivity(i);

            }
        });


        ArrayAdapter<String> daySpinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.day));
        daySpinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);

        spinnerDay.setAdapter(daySpinnerAdapter);
        spinnerDay.setSelection(1);


//        spinnerDay

        initializeTables();

        spinnerDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                try {
                    StudentAsyncTask.cancel(true);
                } catch (Exception e) {

                }
                if (position == 0) {
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(Attendance_marking.this, "Select any class", Toast.LENGTH_LONG).show();
                    setUI(true, false, false, false, false, false, false, true, false);
//
                } else {
                    selectedDivId = (String) arg1.getTag();
                    setUI(true, false, false, false, false, true, false, true, false);
                    Toast.makeText(Attendance_marking.this, "Please Wait", Toast.LENGTH_LONG).show();
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, resultStringList);
                    refreshSubjectFromTable();
                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayAdapter<String> spinnerAdapter;

                if (position == 0) {
//                    Toast.makeText(Attendance_marking.this, "Select Your section", Toast.LENGTH_LONG).show();
                    setUI(true, true, true, false, false, false, false, true, false);

                } else {
                    Section = spinnerDay.getSelectedItem().toString();

                    setUI(true, true, true, true, false, true, true, true, false);
                    checkAll.setVisibility(View.VISIBLE);

                    refreshStudentFromTable();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                ArrayAdapter<String> spinnerAdapter;
                if (position == 0) {
//
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, getResources().getStringArray(R.array.spinner_select_previous));
//                    Toast.makeText(Attendance_marking.this, "Select any subject", Toast.LENGTH_LONG).show();
                    setUI(true, true, false, false, false, false, false, true, false);
                } else {

                    selectsubId = (String) arg1.getTag();
                    Calendar now = Calendar.getInstance();
                    int timeOfDay = now.get(Calendar.HOUR_OF_DAY);

                    if (timeOfDay >= 0 && timeOfDay < 13) {
//                        Toast.makeText(Attendance_marking.this, "Good Morning", Toast.LENGTH_SHORT).show();
                        spinnerDay.setSelection(1);


                    } else if (timeOfDay >= 13 && timeOfDay < 16) {
//                        Toast.makeText(Attendance_marking.this, "Good Afternoon", Toast.LENGTH_SHORT).show();
                        spinnerDay.setSelection(2);


                    } else {
                        spinnerDay.setSelection(0);

                    }

                    setUI(true, true, true, true, false, true, false, true, false);


                    Toast.makeText(Attendance_marking.this, "Please Wait", Toast.LENGTH_LONG).show();
                    // spinnerAdapter = new ArrayAdapter<String>(currentContext, R.layout.spinner_selector, resultStringList);

                }
                //spinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                //  spinnerBranch.setAdapter(spinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        checkAll.setText("Mark all are Absent");
        checkAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (checkAll.isChecked()) {
                    int count = gridViewStudents.getAdapter().getCount();
                    for (int i = 0; i < count; i++) {
                        gridViewStudents.setItemChecked(i, isChecked);
                    }
                } else {

                }
            }
        });
//            @Override
//            public void onC{
//                if (checkAll.isChecked()){
//                    int count = gridViewStudents.getAdapter().getCount();
//                    for (int i=0;i<count;i++){
//gridViewStudents.setItemChecked();
//                    }
//                }else{
//
//                }
//            }
//        });
//        Button btnCheckAll = (Button) findViewById(R.id.button3);
//        btnCheckAll.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                int count = gridViewStudents.getAdapter().getCount();
//                for (int i = 0; i < count; i++) {
//                    LinearLayout itemLayout = (LinearLayout) gridViewStudents.getChildAt(i); // Find by under LinearLayout
//                    CheckBox checkbox = (CheckBox) itemLayout.findViewById(R.id.gridView_ma_Students);
//                    checkbox.setChecked(true);
//                }
//            }
//        });
        Mark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline() == true) {


                    addStudent();

                    //Toast.makeText(Attendance_marking.this,Sid,Toast.LENGTH_LONG).show();
                }
            }

        });


    }

    @Override
    public void onResume() {
        super.onResume();

        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag("Datepickerdialog");


        if (dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = dayOfMonth + "/" + (++monthOfYear) + "/" + year;
//        seldate = new Date(year - 1900, (monthOfYear - 1), dayOfMonth);

        GregorianCalendar cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
        cal.clear();
        cal.set(year, monthOfYear - 1, dayOfMonth);
        seldate = new Date(cal.getTimeInMillis());

//        Log.d("SelectedDate_Normal:", seldate.toString());
//        Log.d("SelectedDate_GMT:", seldate.toGMTString());
//        Log.d("SelectedDate_Locale:", seldate.toLocaleString());

        if (d.compareTo(seldate) < 0) {
            new GreaterDate(currentContext);
        } else {
            tv.setText(date);

            refreshDivisionFromTable();
            setUI(false, false, false, false, true, false, false, true, false);
        }

    }

    private void initializeTables() {
        try {
            StaffDivTable = AzureMobileService.client.getTable("StaffDivision", StaffDivision.class);
            StudentsTable = AzureMobileService.client.getTable("StaffDivisionStudents", StaffDivisionStudents.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkItem(StaffDivisionStudents s) {
//        Sid = Sid + s.mId + "#";
//        Sname = Sname + s.StudentName + "#";
//        Srollno = Srollno + s.rolno + "#";
        //count = count+1;
        absenteesList.add(s.mId);
        absenteesAdapter.add(s);
    }


    public void uncheckItem(StaffDivisionStudents s) {
//        Sid.replace(Sid + s.mId + "#", "");
//
//        Sname = Sname + s.StudentName + "#";
//        Srollno = Srollno + s.rolno + "#";
        //count = count+1;
        absenteesList.remove(s.mId);
        absenteesAdapter.remove(s);
    }

    public void checkallitems(StaffDivisionStudents s) {
        absenteesList.add(s.mId);
        absenteesAdapter.add(s);
    }

    public void refreshSubjectFromTable() {
        StaffDivAsyncTask = new AsyncSubjectLoader();
        StaffDivAsyncTask.execute();
    }

    public void refreshStudentFromTable() {
        StudentAsyncTask = new AsyncStudentLoader();
        StudentAsyncTask.execute();
    }

    public void refreshDivisionFromTable() {


        DivAsyncTask = new AsyncDivisionLoader();
        DivAsyncTask.execute();
//        StaffDivAsyncTask.cancel(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_course_selection, menu);
        return true;
    }


//    public void getStudentgrid() {
//
//        // Get the items that weren't marked as completed and add them in the
//        // adapter
//        studentsAdapter.clear();
//        absenteesAdapter.clear();
//        absenteesList.clear();

    public void addStudent() {
        setUI(false, false, false, false, false, true, false, true, false);


        String dateString = date;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        List<Pair<String, String>> parameters = new AbstractList<Pair<String, String>>() {

            @Override
            public Pair<String, String> get(int i) {
                return null;
            }

            @Override
            public int size() {
                return 0;
            }
        };

        AttendanceMarkDTO item = new AttendanceMarkDTO();
        item.day = Section;
        item.divid = selectedDivId;
        item.Subid = selectsubId;
        item.absenteesList = new ArrayList<String>(absenteesList);
        item.evdate = seldate;
        item.presnt = false;
//        Log.d("ids", Sid);
        HashSet<String> test = absenteesList;

        Log.d("ids", test.toString());
        Log.d("DAte", seldate.toString());


//        Toast.makeText(this, test.toString() + Section, Toast.LENGTH_SHORT).show();

        final ListenableFuture<String> result = AzureMobileService.client.invokeApi("AttendanceMarkDTO", item, String.class);

        Futures.addCallback(result, new FutureCallback<String>() {
            @Override
            public void onFailure(Throwable exc) {
                //TODO
                exc.printStackTrace();
                Log.d("AttendanceMarking", "Fail:" + result);
                setUI(true, true, true, true, true, false, true, true, true);
//                createAndShowDialog((Exception) exc, "Error");
            }

            @Override
            public void onSuccess(String result) {
                //TODO
                new SuccessDailog(currentContext);
                Log.d("AttendanceMarking", "Success:" + result);
                setUI(true, true, true, true, true, false, true, true, true);

            }
        });


    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {

            Toast.makeText(getBaseContext(), "No Data Connection", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    private void setUI(Boolean classLayoutVisible, Boolean subjectLayoutVisible, Boolean dayLayoutVisible,
                       Boolean listGridLayoutVisible, Boolean spinnerProgressVisible, Boolean mainProgressVisible,
                       Boolean markButtonVisible, Boolean datePickTextViewVisible, Boolean checkallVisible) {

        if (class_layout != null) {   //Check that UI is assigned
            if (classLayoutVisible) {
                class_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                class_layout.setVisibility(LinearLayout.GONE);
            }
        }

        if (subject_layout != null) { //Check that UI is assigned
            if (subjectLayoutVisible) {
                subject_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                subject_layout.setVisibility(LinearLayout.GONE);
            }
        }

        if (day_layout != null) {  //Check that UI is assigned
            if (dayLayoutVisible) {
                day_layout.setVisibility(LinearLayout.VISIBLE);
            } else {
                day_layout.setVisibility(LinearLayout.GONE);
            }
        }
        if (list_grid != null) {  //Check that UI is assigned
            if (listGridLayoutVisible) {
                list_grid.setVisibility(LinearLayout.VISIBLE);
            } else {
                list_grid.setVisibility(LinearLayout.GONE);
            }
        }
        if (mProgressBar != null) {  //Check that UI is assigned
            if (spinnerProgressVisible) {
                mProgressBar.setVisibility(LinearLayout.VISIBLE);
            } else {
                mProgressBar.setVisibility(LinearLayout.GONE);
            }
        }
        if (mProgressBar1 != null) {  //Check that UI is assigned
            if (mainProgressVisible) {
                mProgressBar1.setVisibility(LinearLayout.VISIBLE);
            } else {
                mProgressBar1.setVisibility(LinearLayout.GONE);
            }
        }
        if (Mark != null) {  //Check that UI is assigned
            if (markButtonVisible) {
                Mark.setVisibility(LinearLayout.VISIBLE);
            } else {
                Mark.setVisibility(LinearLayout.GONE);
            }
        }
        if (tv != null) {  //Check that UI is assigned
            if (datePickTextViewVisible) {
                tv.setVisibility(LinearLayout.VISIBLE);
            } else {
                tv.setVisibility(LinearLayout.GONE);
            }
        }
        if (checkAll != null) { //Check that UI is assigned
            if (checkallVisible) {
                checkAll.setVisibility(CheckBox.VISIBLE);
            } else {
                checkAll.setVisibility(CheckBox.GONE);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (StaffDivAsyncTask != null)
                StaffDivAsyncTask.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (DivAsyncTask != null)
                DivAsyncTask.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (StudentAsyncTask != null)
                StudentAsyncTask.cancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void selectAllFromList(boolean b) {
        absenteesList.clear();
        absenteesAdapter.clear();
        int cList = studentsAdapter.dataList.size();
        for (int i = 0; i < cList; i++) {
            View sView = gridViewStudents.getChildAt(i);
            StaffDivisionStudents item = studentsAdapter.dataList.get(i);
            studentsAdapter.dataList.set(i, item);
            item.statusChecked = b;
            if (b)
                checkItem(item);
            else
                uncheckItem(item);

            if (sView != null) {
                CheckBox childCheckBox = (CheckBox) sView.findViewById(R.id.chkbox);
//                cb.setTag(item);
                childCheckBox.setChecked(b);
//                childCheckBox.setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View v) {
//                        if (((CheckBox) v).isChecked()) {
//                            cb.setChecked(false);
//                            StaffDivisionStudents item = (StaffDivisionStudents)cb.getTag();
//
//                            checkItem(item);
//                        }else {
//
//                        }
//                    }
//                });
            }
        }
    }

    private class AsyncStudentLoader extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            studentsAdapter.clear();
            absenteesAdapter.clear();
            absenteesList.clear();
            setUI(true, true, true, false, false, true, false, true, false);

        }

        @Override

        protected Void doInBackground(Void... params) {

            try {

                final List<StaffDivisionStudents> results =
                        StudentsTable.where().field("Division_Id").eq(selectedDivId).orderBy("roll_No", QueryOrder.Ascending).execute().get();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        // mSwipeLayout.setRefreshing(false);
                        if (results.size() == 0) {

                            setUI(true, true, false, false, false, false, false, true, false); // student list updation failed
                            Toast.makeText(Attendance_marking.this, "No students", Toast.LENGTH_LONG).show();
                        } else {
                            studentsAdapter.clear();
                            absenteesAdapter.clear();
                            absenteesList.clear();
                            studentsAdapter.dataList = results;
                            studentsAdapter.addAll(results);
                            setUI(true, true, true, true, false, false, true, true, true);   // student list loaded

//                                for (StaffDivisionStudents item : results) {
//
//                                    studentsAdapter.add(item);
//                                    // pull.setVisibility(View.GONE);
//                                }


                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();


                // createAndShowDialog(e, "Error");


            }
            return null;
        }

        protected void onPostExecute(Void results) {
            super.onPostExecute(results);
            setUI(true, true, true, true, false, false, true, true, true);
        }
    }

    private class AsyncSubjectLoader extends AsyncTask<Void, Void, List<StaffDivision>> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected List<StaffDivision> doInBackground(Void... params) {
            List<StaffDivision> results = null;
            try {
                results = StaffDivTable.where().field("Division_Id").eq(selectedDivId).execute().get();

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<StaffDivision> courseDTOs) {
            super.onCancelled(courseDTOs);

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

        }

        @Override
        protected void onPostExecute(List<StaffDivision> departmentItemDTOs) {
            super.onPostExecute(departmentItemDTOs);

            if (departmentItemDTOs != null && !departmentItemDTOs.isEmpty()) //Successfull results
            {
                StaffDivision dummySelector = new StaffDivision(); //Used for showing the first element to be a prompt
                dummySelector.Subname = "Select your Subject";
                //
                // dummySelector.courseId = "none";
                departmentItemDTOs.add(0, dummySelector);

                StaffDivAdapter ClassAdapter = new StaffDivAdapter(Attendance_marking.this, R.layout.spinner_selector, R.layout.spinner_dropdown, departmentItemDTOs);
                spinnerSubject.setAdapter(ClassAdapter);
                setUI(true, true, false, false, false, false, false, true, false);
//                sp_Department.setSelection(-1);

                // Toast.makeText(getBaseContext(), "Please Select your Course", Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(getBaseContext(), "Error retrieving courses, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }


        }
    }

    private class AsyncDivisionLoader extends AsyncTask<Void, Void, List<StaffDivision>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected List<StaffDivision> doInBackground(Void... params) {
            List<StaffDivision> results = null;
            try {
                try {
                    results = StaffDivTable.execute().get();
                } catch (MobileServiceException e) {
                    e.printStackTrace();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void onCancelled(List<StaffDivision> courseDTOs) {
            super.onCancelled(courseDTOs);

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

        }

        @Override
        protected void onPostExecute(List<StaffDivision> ClassitemDTO) {
            super.onPostExecute(ClassitemDTO);

            if (ClassitemDTO != null && !ClassitemDTO.isEmpty()) //Successfull results
            {
                StaffDivision dummySelector = new StaffDivision(); //Used for showing the first element to be a prompt
                dummySelector.DivName = "Select your Class";
                dummySelector.Divid = "none";
                ClassitemDTO.add(0, dummySelector);

                DivAdapter depAdapter = new DivAdapter(Attendance_marking.this, R.layout.spinner_selector, R.layout.spinner_dropdown, ClassitemDTO);
                spinnerDivision.setAdapter(depAdapter);
//                sp_Department.setSelection(-1);
                setUI(true, false, false, false, false, false, false, true, false);


            } else {
                Toast.makeText(getBaseContext(), "Error retrieving courses, Probably bad network connection!", Toast.LENGTH_LONG).show();
            }


        }

    }

}
