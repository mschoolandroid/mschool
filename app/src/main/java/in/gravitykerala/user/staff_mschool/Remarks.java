package in.gravitykerala.user.staff_mschool;

import java.util.Date;

/**
 * Created by USER on 1/9/2016.
 */
public class Remarks {

    @com.google.gson.annotations.SerializedName("student_Id")
    public String sId;
    @com.google.gson.annotations.SerializedName("teacher_Id")
    public String Techerid;
    @com.google.gson.annotations.SerializedName("description")
    public String Dsc;
    @com.google.gson.annotations.SerializedName("event_date")
    public Date evdte;
    @com.google.gson.annotations.SerializedName("reviewed")
    public Boolean rvwd;

    @com.google.gson.annotations.SerializedName("id")
    private String mId;

}
