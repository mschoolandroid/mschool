package in.gravitykerala.user.staff_mschool;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import in.gravitykerala.user.staff_mschool.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Adapter to bind a ToDoItem List to a view
 */
public class PresentDayListAdapter extends ArrayAdapter<StaffStudentIndividualDTO.Single> {

    /**
     * Adapter context
     */
    Context mContext;


    /**
     * Adapter View layout
     */
    int mLayoutResourceId;

    public PresentDayListAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mContext = context;
        mLayoutResourceId = layoutResourceId;
    }

    /**
     * Returns the view for a specific item on the list
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        final StaffStudentIndividualDTO.Single currentItem = getItem(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(mLayoutResourceId, parent, false);
        }

        row.setTag(currentItem);
//        final TextView tv = (TextView) row.findViewById(R.id.name);
        final TextView tv1 = (TextView) row.findViewById(R.id.date);


        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date convertedDate = currentItem.evdate;
        String newdt = dateFormat.format(convertedDate);
        try {
            convertedDate = dateFormat.parse(currentItem.evdate.toString());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
//        final TextView tv2 = (TextView) row.findViewById(R.id.stats);
//        tv.setText("" + currentItem.studnam);
        tv1.setText(newdt);
//        tv2.setText("" + currentItem.status);
        final String date = tv1.getText().toString();
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, StudentListSelect.class);
                i.putExtra("date", currentItem.evdate.toString());
                i.putExtra("id", StudentsingleSelect.id);
                i.putExtra("divsionid", StudentsingleSelect.divid);
                i.putExtra("subid", StudentsingleSelect.subid);
                mContext.startActivity(i);
            }
        });
        return row;
    }
}
