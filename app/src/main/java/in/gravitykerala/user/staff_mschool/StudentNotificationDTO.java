package in.gravitykerala.user.staff_mschool;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by USER on 7/10/2015.
 */
public class StudentNotificationDTO {
    @com.google.gson.annotations.SerializedName("id")
    public String mId;
    ArrayList<Exam> staffs;

    StudentNotificationDTO() {
        staffs = new ArrayList<Exam>();

    }

    public Exam getName(int i) {
        return staffs.get(i);
    }

    public String getId() {
        return mId;
    }

    public class Exam {
        @com.google.gson.annotations.SerializedName("type")
        public String type;
        @com.google.gson.annotations.SerializedName("event_Date")
        public Date date;

        @com.google.gson.annotations.SerializedName("title")
        public String title;
        @com.google.gson.annotations.SerializedName("content")
        public String content;


        public String getId() {
            return mId;
        }
    }

}
