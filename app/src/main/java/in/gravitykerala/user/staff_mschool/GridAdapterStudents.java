package in.gravitykerala.user.staff_mschool;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import in.gravitykerala.user.staff_mschool.R;

import java.util.List;

/**
 * Adapter to bind a ToDoItem List to a view
 */
public class GridAdapterStudents extends ArrayAdapter<StaffDivisionStudents> {

    public List<StaffDivisionStudents> dataList;
    /**
     * Adapter context
     */
    boolean isAllCHecked = false;

    Context mContext;
    /**
     * Adapter View layout
     */
    int mLayoutResourceId;

    public GridAdapterStudents(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mContext = context;
        mLayoutResourceId = layoutResourceId;
    }

    /**
     * Returns the view for a specific item on the list
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;

        final StaffDivisionStudents currentItem = dataList.get(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(mLayoutResourceId, parent, false);
//
        }


        final CheckBox checkBox = (CheckBox) row.findViewById(R.id.chkbox);
        final TextView st_name = (TextView) row.findViewById(R.id.name_nm);
        checkBox.setText("" + currentItem.rolno);
        checkBox.setChecked(currentItem.statusChecked);
        currentItem.position = position;
        dataList.set(position, currentItem);
        row.setTag(currentItem);
//        checkBox.setEnabled(true);

//        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                currentItem.statusChecked = isChecked;
//                if (mContext instanceof Attendance_marking) {
//                    Attendance_marking activity = (Attendance_marking) mContext;
//
//                    if(isChecked)
//                        activity.checkItem(currentItem);
//                    else
//                        activity.uncheckItem(currentItem);
//                }
//            }
//        });
        st_name.setText(currentItem.StudentName);
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (checkBox.isChecked()) {
                    currentItem.statusChecked = true;
                    if (mContext instanceof Attendance_marking) {
                        Attendance_marking activity = (Attendance_marking) mContext;

                        activity.checkItem(currentItem);
                    }

                } else if (!checkBox.isChecked()) {
                    currentItem.statusChecked = false;
                    if (mContext instanceof Attendance_marking) {
                        Attendance_marking activity = (Attendance_marking) mContext;
                        activity.uncheckItem(currentItem);
                    }
                }
                dataList.set(position, currentItem);
            }
        });
        return row;
    }


}
